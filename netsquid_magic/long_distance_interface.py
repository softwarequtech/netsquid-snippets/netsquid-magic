from __future__ import annotations
from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from netsquid.qubits import QRepr


class ILongDistanceInterface(ABC):
    @abstractmethod
    def operate(self, state: QRepr) -> QRepr:
        pass

    @property
    @abstractmethod
    def probability_success(self) -> float:
        pass
