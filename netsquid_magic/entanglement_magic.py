from __future__ import annotations
from collections import deque
from typing import Dict, TYPE_CHECKING
import logging

from netsquid.protocols.nodeprotocols import NodeProtocol

from netsquid_driver.classical_routing_service import ClassicalRoutingService
from netsquid_driver.entanglement_agreement_service import EntanglementAgreementService, \
    ReqEntanglementAgreement, ResEntanglementAgreementRejected, ResEntanglementAgreementReached, \
    signal_entanglement_agreement_status_change, ReqEntanglementAgreementAbort
from netsquid_driver.entanglement_service import EntanglementService, ResEntanglementError, EntanglementError, \
    ResEntanglementSuccess, ResEntanglementFail, ResStateReady, ReqEntanglement

if TYPE_CHECKING:
    from netsquid_magic.qlink import MagicQLink

logger = logging.getLogger(__name__)


class EntanglementMagic(EntanglementService):
    """Implementation of EntanglementService by using MagicQLink (that use MagicDistributors)."""

    class EntanglementSubprotocol(NodeProtocol):
        def __init__(self, remote_node_name: str, remote_node_id: int, qlink: MagicQLink, superprotocol):
            super().__init__(node=superprotocol.node, name=f"entanglement_subprotocol_{superprotocol.node.name}_{remote_node_name}")
            self.remote_node_name = remote_node_name
            self._remote_node_id = remote_node_id
            self._qlink = qlink
            self.superprotocol = superprotocol
            self.req = None
            self._connection_id = 0
            self._reached_agreement = False
            self._waiting_for_agreement = False
            self._qlink.add_callback(self._handle_state_delivery, event_type="state_delivery")
            self._qlink.add_callback(self._handle_label_delivery, event_type="label_delivery")

        def start(self, req: ReqEntanglement):
            assert req.remote_node_name == self.remote_node_name
            self.req = req
            self.superprotocol.set_in_use(req.mem_pos, True)
            self.superprotocol.logger.debug(f"Start subprotocol for request: {req}")

            super().start()

        def run(self):
            self._reached_agreement = False
            yield from self.reach_agreement()
            if self._reached_agreement:
                self.superprotocol.logger.debug(f"Reached entanglement agreement with {self.remote_node_name},"
                                                f" starting entanglement generation")
                yield from self.attempt_entanglement_generation_until_success()

        def reach_agreement(self):
            entanglement_agreement_service = self.node.driver[EntanglementAgreementService]
            agreement_req = ReqEntanglementAgreement(remote_node_name=self.remote_node_name)
            entanglement_agreement_service.put(agreement_req)
            self._waiting_for_agreement = True
            evt_rejected = self.await_signal(sender=entanglement_agreement_service,
                                             signal_label=ResEntanglementAgreementRejected.__name__)
            evt_agreement_reached = self.await_signal(sender=entanglement_agreement_service,
                                                      signal_label=ResEntanglementAgreementReached.__name__)
            while True:
                evt_expr = yield evt_rejected | evt_agreement_reached
                [evt] = evt_expr.triggered_events
                res = entanglement_agreement_service.get_signal_by_event(evt).result
                if res.remote_node_name == self.remote_node_name and res.connection_id == self._connection_id:
                    self._waiting_for_agreement = False
                    break
            if isinstance(res, ResEntanglementAgreementRejected):
                self._handle_agreement_rejection()
            elif isinstance(res, ResEntanglementAgreementReached):
                self._handle_agreement_reached()
            else:
                raise RuntimeError("Received unexpected signal.")

        def _handle_agreement_rejection(self):
            self.superprotocol.queue.append(self.req)
            self.superprotocol._requests_with_agreement_rejected.append(self.req)
            self.superprotocol.set_in_use(self.req.mem_pos, False)

        def _handle_agreement_reached(self):
            self._reached_agreement = True

        def attempt_entanglement_generation_until_success(self):
            """Perform entanglement attempts until a success is obtained."""
            # define relevant events
            evt_succ = self.await_signal(sender=self.superprotocol,
                                         signal_label=ResEntanglementSuccess.__name__)
            evt_fail = self.await_signal(sender=self.superprotocol,
                                         signal_label=ResEntanglementFail.__name__)
            evt_error = self.await_signal(sender=self.superprotocol,
                                          signal_label=ResEntanglementError.__name__)
            while True:  # keep attempting entanglement until a success is obtained

                # start an attempt at entanglement generation
                # how an entanglement attempt is performed, must be defined in the superprotocol
                self.attempt_entanglement_generation(remote_node_name=self.remote_node_name,
                                                     mem_pos=self.req.mem_pos)
                # keep listening for responses until a response corresponding to this port/mem_pos obtained
                # that is not a state-ready response
                while True:
                    # wait for success, failure or error
                    evt_expr = yield evt_succ | evt_fail | evt_error
                    [evt] = evt_expr.triggered_events
                    res = self.superprotocol.get_signal_by_event(evt).result
                    # check if response corresponds to entanglement generation this protocol is working on
                    if res.remote_node_name == self.remote_node_name:
                        assert res.mem_pos == self.req.mem_pos  # should only be one entanglement generation per port
                        break

                # terminate protocol if success
                if isinstance(res, ResEntanglementSuccess):
                    self.superprotocol.logger.info(f"Successfully finished request: {self.req}")
                    break

                # raise error if error response
                if isinstance(res, ResEntanglementError):
                    raise RuntimeError(f"Exited with error code {res.error_code}.")

                # else, it was a failure and we try entanglement generation again.
                self.superprotocol.logger.info(f"Failed entanglement generation, trying again for request: {self.req}")

        def abort(self):
            self.superprotocol.logger.debug(f"Abort subprotocol for request: {self.req}")
            self._qlink.abort()
            self.stop()

        def stop(self):
            """When this protocol is stopped, the superprotocol should check its queue since new space freed up."""
            super().stop()
            if self._waiting_for_agreement:
                self.node.driver[EntanglementAgreementService].put(
                    ReqEntanglementAgreementAbort(remote_node_name=self.remote_node_name))
                self._waiting_for_agreement = False

            self.superprotocol.logger.debug(f"Stop subprotocol for request: {self.req}")
            # TODO We need to add a system that reacts to aborted entanglement agreements
            self.superprotocol.send_signal(self.superprotocol._check_queue_signal)
            self.req = None

        @property
        def is_connected(self):
            """The protocol is only connected if the node has a :class:`Driver` attached to it,
            with a registered :class:`ClassicalRoutingService`."""
            try:
                assert self.node.driver[ClassicalRoutingService]
                assert self.node.driver[EntanglementAgreementService]
            except (AttributeError, KeyError):
                return False
            return super().is_connected

        def _handle_state_delivery(self, event):
            delivery = self._qlink.peek_node_delivery(event)
            if delivery.node_id == self.node.ID:
                self.superprotocol.logger.debug(f"State delivery with remote node: {self.remote_node_name}"
                                                f" at mempos {self.req.mem_pos}")
                self.superprotocol.send_response(ResStateReady(remote_node_name=self.remote_node_name,
                                                               mem_pos=self.req.mem_pos))

        def _handle_label_delivery(self, event):
            delivery = self._qlink.peek_node_delivery(event)
            if delivery.node_id == self.node.ID:
                bell_state_index = self._qlink.get_bell_state()
                self.superprotocol.logger.debug(f"label delivery with remote node: {self.remote_node_name} "
                                                f"at mempos: {self.req.mem_pos} with bell state: {bell_state_index}")

                self.superprotocol.send_response(ResEntanglementSuccess(remote_node_id=self._remote_node_id,
                                                                        remote_node_name=self.remote_node_name,
                                                                        mem_pos=self.req.mem_pos,
                                                                        bell_index=bell_state_index))

        def attempt_entanglement_generation(self, remote_node_name, mem_pos):
            self._qlink.add_pair_request(senderID=self.node.ID,
                                         remoteID=self._remote_node_id,
                                         pos_sender=mem_pos)

    class ListenForUpdatesInEntanglementAgreementServiceSubprotocol(NodeProtocol):
        """Subprotocol that listens to :class:`~EntanglementAgreementService` for update signals.

        If a `entanglement_agreement_service.signal_entanglement_agreement_status_change` is received,
        this means that the status of `EntanglementAgreementService` has somehow changed.
        Therefore, it might have become possible to reach agreement with other nodes about resolving entanglement
        requests where it was previously impossible.

        Requests that were previously marked as "rejected" are unmarked, and the queue is checked.

        """
        def __init__(self, superprotocol):
            super().__init__(node=superprotocol.node,
                             name="listen_for_updates_in_entanglement_agreement_service_subprotocol")
            self.superprotocol = superprotocol

        def run(self):
            agreement_service = self.node.driver[EntanglementAgreementService]
            while True:
                yield self.await_signal(sender=agreement_service,
                                        signal_label=signal_entanglement_agreement_status_change)
                self.superprotocol._requests_with_agreement_rejected = []
                self.superprotocol.check_queue()

    def __init__(self, node,
                 qlink_dict: Dict[str, MagicQLink],
                 node_name_id_mapping: Dict[str, int],
                 num_parallel_links=None, name=None):
        super().__init__(node=node, name=name)
        self.num_parallel_attempts = num_parallel_links
        self.queue = deque()
        self._qlink_dict = qlink_dict
        self._node_name_id_mapping = node_name_id_mapping
        self._check_queue_signal = "check_queue"
        self.add_signal(self._check_queue_signal)
        self._requests_with_agreement_rejected = []  # request for which it has not been possible to reach agreement
        self.logger = logger.getChild(f"{node.name}")

    def set_in_use(self, mem_pos, value):
        """Set memory position's "in use" status, which serves to avoid it from being used by other entities.

        Must be overwritten to use. Queue is automatically checked if this method is used to free a memory position.

        Parameters
        ----------
        mem_pos : int
            Memory position of which the "in use" status must be set.
        value : bool
            True if set to "in use", False if set to "available".

        """
        self.node.qmemory.mem_positions[mem_pos].in_use = value
        if not value:
            self.send_signal(self._check_queue_signal)

    def get_in_use(self, mem_pos):
        return self.node.qmemory.mem_positions[mem_pos].in_use

    def start(self):
        """Every port on the node needs its own subprotocol. Before starting, any missing subprotocols are created."""
        self.add_subprotocol(self.ListenForUpdatesInEntanglementAgreementServiceSubprotocol(superprotocol=self),
                             name=self.ListenForUpdatesInEntanglementAgreementServiceSubprotocol.__name__)
        for remote_node_name, magic_distributor in self._qlink_dict.items():
            subprot_name = self._get_subprot_name_by_remote_node_name(remote_node_name)
            remote_node_id = self._node_name_id_mapping[remote_node_name]
            if subprot_name not in self.subprotocols:
                self.add_subprotocol(self.EntanglementSubprotocol(remote_node_name=remote_node_name,
                                                                  qlink=magic_distributor,
                                                                  remote_node_id=remote_node_id,
                                                                  superprotocol=self),
                                     name=subprot_name)
        super().start()

    def run(self):
        """Distribute entanglement requests over subprotocols.

        This is done in such a way that
        1) the total number of simultaneous entanglement generations never exceeds the set maximum and
        2) entanglement generation is only attempted once per port at a time.
        Requests that cannot be handled are kept in a queue and are reconsidered whenever a new request is made or
        one of the entanglement attempts succeeds (freeing up room).

        """

        self.subprotocols[self.ListenForUpdatesInEntanglementAgreementServiceSubprotocol.__name__].start()
        while True:
            # run through the queue every time a check_queue_signal is sent
            # this should happen every time a new entanglement request is made,
            # and every time entanglement has been generated successfully (i.e. when a subprotocol terminates)
            yield self.await_signal(sender=self, signal_label=self._check_queue_signal)
            self._execute_as_many_requests_from_queue_as_possible()

    def _get_subprot_name_by_remote_node_name(self, remote_node_name: str) -> str:
        return f"heralded_entanglement_subprotocol_{self.node.name}_{remote_node_name}"

    def _execute_as_many_requests_from_queue_as_possible(self):
        """Execute as many entanglement generation requests as possible. Treats the queue in order.
        Requests which cannot be processed due to insufficient resources are put back at the end of the queue.
        """

        unexecuted_requests = []
        while len(self.queue) != 0:
            req = self.queue.popleft()

            is_executed = self._try_to_execute_request_and_return_whether_executing(req=req)
            if not is_executed:
                unexecuted_requests.append(req)

        for req in unexecuted_requests:
            self.queue.append(req)  # put request back into queue to execute later

    def _try_to_execute_request_and_return_whether_executing(self, req):
        """
        Returns
        -------
        bool
            Whether the entanglement generation request is executed.
        """
        if self._can_execute_request(req):
            self._execute_request(req)
            return True
        else:
            return False

    def _execute_request(self, req):
        """Execute an entanglement generation request.

        Execution of individual requests is resolved by
        :class:`entanglement_service.HeraldedEntanglementBaseProtocol.HeraldedEntanglementSubprotocol` instances.
        This involves first trying to get agreement with the remote node.
        If agreement is reached, entanglement generation is attempted until a success is obtained.
        If no agreement is reached, the request is put back into the queue.

        """
        # subprotocol handling entanglement on this specific port
        subprot = self._get_subprot_by_remote_node_name(req.remote_node_name)

        # start execution of request
        subprot.start(req=req)

    def _can_execute_request(self, req):
        """Checks whether request can be executed, which it cannot if:

        - if memory position is already in use
        - if port is already in use (i.e. if the relevant subprotocol on the port is already running)
        - agreement with remote node is impossible
        - all possible parallel attempt-paths are taken

        Returns
        -------
        bool
            True if request can be executed, False if not.

        """
        mem_pos_is_free = not self.get_in_use(req.mem_pos)
        subprot_is_free = not self._get_subprot_by_remote_node_name(req.remote_node_name).is_running
        agreement_is_possible = req not in self._requests_with_agreement_rejected
        not_all_parallel_attempts_taken = not self._are_all_parallel_attempts_taken()
        return mem_pos_is_free and subprot_is_free and agreement_is_possible and not_all_parallel_attempts_taken

    def _get_subprot_by_remote_node_name(self, remote_node_name) -> EntanglementMagic.EntanglementSubprotocol:
        return self.subprotocols[self._get_subprot_name_by_remote_node_name(remote_node_name)]

    def _are_all_parallel_attempts_taken(self):
        """
        Returns
        -------
        bool
        """
        if self.num_parallel_attempts is None:
            return False
        else:
            return self.num_parallel_attempts <= self._num_running_entanglement_attempts

    def generate_entanglement(self, req):
        """Entanglement requests are put in a queue and handled in the `run` method.

        The protocol is started if it is not yet running.

        Parameters
        ----------
        req : :class:`ReqEntanglement`
            Request that needs to be handled by this method.

        """
        super().generate_entanglement(req)
        if not self.is_running:
            self.start()
        self.queue.append(req)
        self.send_signal(self._check_queue_signal)

    def abort(self, req):
        """Abort entanglement generation.

        This method ensures that no new entanglement attempts are made to the remote node specified by the requests and
        sends off a :class:`ResEntanglementError` response so that other nodes may also be aware that entanglement
        generation on said remote node is no longer underway.

        Parameters
        ----------
        req : :class:`ReqEntanglementAbort`
            Request that needs to be handled by this method.

        Note
        ----
        If an implementation of this base protocol should also abort entanglement attempts already underway,
        this method can be overwritten, but make sure it also calls `super().abort(req)` so that the queue is cleared.

        """
        super().abort(req)

        # remove all entanglement requests from queue that correspond to the remote node name of abort request
        for _ in range(len(self.queue)):
            queue_req = self.queue.popleft()
            if not queue_req.remote_node_name == req.remote_node_name:
                self.queue.append(queue_req)

        # stop subprotocol dealing with entanglement generation on port name of abort request
        subprot = self._get_subprot_by_remote_node_name(req.remote_node_name)
        if subprot.req:
            self.set_in_use(subprot.req.mem_pos, False)
        subprot.abort()

        # check queue (maybe some requests can now be handled that could not be before)
        self.send_signal(self._check_queue_signal)

        # the information about the memory position included in the response is currently not used and is hard to get.
        # we set it to a non-sensical value so as to make clear it is not the actual memory position
        response = ResEntanglementError(error_code=EntanglementError.ABORTED, remote_node_name=req.remote_node_name,
                                        mem_pos=-1)
        self.send_signal(ResEntanglementError.__name__, result=response)

    def stop(self):
        """Stop the protocol and clear the queue."""
        self.queue.clear()
        super().stop()

    @property
    def is_connected(self):
        """The protocol is only connected if the node has a :class:`Driver` attached to it,
        with a registered :class:`ClassicalRoutingService`."""
        if self.node.qmemory is None:
            return False
        try:
            assert self.node.driver[ClassicalRoutingService]
        except (AttributeError, KeyError):
            return False
        return super().is_connected

    def check_queue(self):
        """Check and, if possible, handle queued requests.

        This method is meant for use by other entities. While the queue is automatically checked whenever a request is
        made or entanglement is generated successfully or aborted, it can be the case that the only requests left in
        the queue cannot be handled because memory positions are in use. When they are freed, the queue is not
        automatically checked again. In that case, this method can be called.

        """
        self.send_signal(self._check_queue_signal)

    @property
    def _num_running_entanglement_attempts(self):
        """Number of subprotocols that are currently running."""
        running_subprots = {subprot for name, subprot in self.subprotocols.items()
                            if subprot.is_running}
        entanglement_subprots = {self._get_subprot_by_remote_node_name(remote_node_name) for remote_node_name in self._qlink_dict.keys()}
        running_entanglement_subprots = running_subprots & entanglement_subprots
        return len(running_entanglement_subprots)
