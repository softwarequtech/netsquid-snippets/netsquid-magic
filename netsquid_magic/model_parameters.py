import dataclasses
import numbers
import numpy
from abc import ABCMeta, abstractmethod


@dataclasses.dataclass
class IModelParameters(metaclass=ABCMeta):
    """Abstract base data class for the parameters of entanglement generation models."""
    cycle_time: float = 0
    """Duration [ns] of each round of entanglement distribution."""

    @abstractmethod
    def verify(self):
        self.verify_not_negative_value("cycle_time")

    def verify_equal(self, param_name: str, expected_value):
        val = self.__getattribute__(param_name)
        self._raise_error_if_none_not_allowed(param_name, val, can_be_none=False)
        if not numpy.isclose(val, expected_value, atol=0, rtol=1e-9):
            raise ValueError(f"{self._error_msg_base(param_name)} Expected value {expected_value}, but got {val}")

    def verify_not_negative_value(self, param_name: str, can_be_none=False):
        val = self.__getattribute__(param_name)
        if val is not None:
            self._verify_is_real_number(param_name, val)
            if val < 0:
                raise ValueError(f"{self._error_msg_base(param_name)} Expecting non-negative value, but got: {val}")
        self._raise_error_if_none_not_allowed(param_name, val, can_be_none)

    def verify_between_0_and_1(self, param_name: str, can_be_none=False):
        val = self.__getattribute__(param_name)
        if val is not None:
            self._verify_is_real_number(param_name, val)
            if val < 0 or val > 1:
                raise ValueError(f"{self._error_msg_base(param_name)} Expecting value between 0 and 1, but got {val}")
        self._raise_error_if_none_not_allowed(param_name, val, can_be_none)

    def verify_is_real_number(self, param_name: str, can_be_none=False):
        val = self.__getattribute__(param_name)
        if val is not None:
            self._verify_is_real_number(param_name, val)
        self._raise_error_if_none_not_allowed(param_name, val, can_be_none)

    def verify_is_type(self, param_name: str, expected_type: type, can_be_none=False):
        val = self.__getattribute__(param_name)
        if val is not None:
            if not isinstance(val, expected_type):
                raise ValueError(f"{self._error_msg_base(param_name)} Expecting type: {expected_type}, but got {type(val)}")
        self._raise_error_if_none_not_allowed(param_name, val, can_be_none)

    def _error_msg_base(self, param_name) -> str:
        return f"Verification of ModelParameters failed for model: {type(self)} and parameter: {param_name};"

    def _raise_error_if_none_not_allowed(self, param_name: str, value, can_be_none=False):
        if value is None and not can_be_none:
            raise ValueError(f"{self._error_msg_base(param_name)} Value is None")

    def _verify_is_real_number(self, param_name: str, value):
        if not isinstance(value, numbers.Real):
            raise ValueError(f"{self._error_msg_base(param_name)} Value: {value} is not a number")


@dataclasses.dataclass
class PerfectModelParameters(IModelParameters):
    """Data class for the parameters of the 'perfect' entanglement generation model."""
    state_delay: float = 0
    """The time [ns] it takes to generate a perfect EPR state."""

    def verify(self):
        self.verify_equal("cycle_time", 0)
        self.verify_not_negative_value("state_delay")


@dataclasses.dataclass
class DepolariseModelParameters(IModelParameters):
    """Data class for the parameters of the depolarising entanglement generation model."""
    prob_max_mixed: float = 0
    """Fraction of maximally mixed state in the EPR state generated."""
    prob_success: float = 1
    """Probability of successfully generating an EPR state per cycle."""
    random_bell_state: bool = False
    """Determines whether the Bell state is always phi+ or randomly chosen from phi+, phi-, psi+, or psi-."""

    def verify(self):
        super().verify()
        self.verify_between_0_and_1("prob_max_mixed")
        self.verify_between_0_and_1("prob_success")
        self.verify_is_type("random_bell_state", bool)


@dataclasses.dataclass
class BitFlipModelParameters(IModelParameters):
    """Data class for the parameters of the bit flip entanglement generation model."""
    flip_prob: float = 0
    """
    Probability that instead of a perfectly correlated EPR pair,
    a perfectly anti-correlated pair is distributed.
    """

    def verify(self):
        super().verify()
        self.verify_between_0_and_1("flip_prob")


@dataclasses.dataclass
class HeraldedModelParameters(IModelParameters, metaclass=ABCMeta):
    """Abstract base data class for the parameters of heralded entanglement generation models."""
    cycle_time: float = None
    """Duration [ns] of each round of entanglement distribution.
    Will be calculated based on other parameters if None."""
    length_A: float = None
    """Length [km] of fiber on "A" side of heralded connection."""
    length_B: float = None
    """Length [km] of fiber on "B" side of heralded connection."""
    speed_of_light_A: float = 200000
    """Speed of light [km/s] in fiber on "A" side of heralded connection. Defaults to 200,000 km/s."""
    speed_of_light_B: float = 200000
    """Speed of light [km/s] in fiber on "B" side of heralded connection. Defaults to 200,000 km/s."""
    emission_duration_A: float = 0
    """Time [ns] it takes the memory on the "A" side to emit a photon that is entangled with a memory qubit."""
    emission_duration_B: float = 0
    """Time [ns] it takes the memory on the "B" side to emit a photon that is entangled with a memory qubit."""
    p_loss_length_A: float = .25
    """Attenuation coefficient [dB/km] of fiber between "A" and midpoint. Defaults to 0.25 dB/km"""
    p_loss_length_B: float = .25
    """Attenuation coefficient [dB/km] of fiber between "B" and midpoint. Defaults to 0.25 dB/km"""
    p_loss_init_A: float = 0
    """Probability that a photon gets lost when entering heralded connection on "A" side."""
    p_loss_init_B: float = 0
    """Probability that a photon gets lost when entering heralded connection on "B" side."""
    collection_efficiency_A: float = 1
    """Chance of collecting the entangled photon emitted by a memory qubit on "A" side.
    This parameter wil only rescale `p_loss_init`."""
    collection_efficiency_B: float = 1
    """Chance of collecting the entangled photon emitted by a memory qubit on "B" side.
    This parameter wil only rescale `p_loss_init`."""

    _has_absorbed_collection_efficiency_in_p_init = False

    @abstractmethod
    def verify(self):
        super().verify()
        self.verify_only_heralded_params()

    def verify_only_heralded_params(self):
        self.verify_not_negative_value("length_A")
        self.verify_not_negative_value("length_B")
        self.verify_not_negative_value("speed_of_light_A")
        self.verify_not_negative_value("speed_of_light_B")
        self.verify_not_negative_value("emission_duration_A")
        self.verify_not_negative_value("emission_duration_B")
        self.verify_not_negative_value("p_loss_length_A")
        self.verify_not_negative_value("p_loss_length_B")
        self.verify_between_0_and_1("p_loss_init_A")
        self.verify_between_0_and_1("p_loss_init_B")
        self.verify_between_0_and_1("collection_efficiency_A")
        self.verify_between_0_and_1("collection_efficiency_B")

    def absorb_collection_efficiency_in_p_init(self):
        if self._has_absorbed_collection_efficiency_in_p_init:
            raise RuntimeError("Method to absorb collection efficiency into p_init factor has been called twice")
        self._has_absorbed_collection_efficiency_in_p_init = True

        self.p_loss_init_A = (1 - (1 - self.p_loss_init_A) * self.collection_efficiency_A)
        self.p_loss_init_B = (1 - (1 - self.p_loss_init_B) * self.collection_efficiency_B)


@dataclasses.dataclass
class SingleClickModelParameters(HeraldedModelParameters):
    """Data class for the parameters of the single click entanglement generation model."""
    detector_efficiency: float = 1
    """Probability that the presence of a photon leads to a detection event."""
    visibility: float = 1
    """Hong-Ou-Mandel visibility of photons that are being interfered (measure of photon indistinguishability)."""
    dark_count_probability: float = 0
    """Dark-count probability per detection."""
    coherent_phase: float = 0
    """In the absence of all other noise, this sampler produces the state
    :math:`|01\\rangle + e^{i \\text{coherent_phase}} |10\\rangle`
    """
    num_resolving: bool = False
    """Determines whether photon-number-resolving detectors are used for the Bell-state measurement."""
    bright_state_parameter_A: float = None
    """Determines the fraction of the matter qubit state that is in the up state.
    This affects the matter qubit on the A side."""
    bright_state_parameter_B: float = None
    """Determines the fraction of the matter qubit state that is in the up state.
    This affects the matter qubit on the B side."""

    def verify(self):
        super().verify()
        self.verify_between_0_and_1("detector_efficiency")
        self.verify_between_0_and_1("visibility")
        self.verify_between_0_and_1("dark_count_probability")
        self.verify_is_real_number("coherent_phase")
        self.verify_is_type("num_resolving", bool)
        self.verify_between_0_and_1("bright_state_parameter_A", can_be_none=True)
        self.verify_between_0_and_1("bright_state_parameter_B", can_be_none=True)


@dataclasses.dataclass
class DoubleClickModelParameters(HeraldedModelParameters):
    """Data class for the parameters of the double click entanglement generation model."""
    detector_efficiency: float = 1
    """Probability that the presence of a photon leads to a detection event."""
    dark_count_probability: float = 0
    """Dark-count probability per detection."""
    visibility: float = 1
    """Hong-Ou-Mandel visibility of photons that are being interfered (measure of photon indistinguishability)."""
    num_resolving: bool = False
    """Determines whether photon-number-resolving detectors are used for the Bell-state measurement."""
    num_multiplexing_modes: int = 1
    """Number of modes used for multiplexing, i.e. how often entanglement generation is attempted per round."""
    emission_fidelity_A: float = 1
    """Fidelity of state shared between photon and memory qubit on "A" side to
    :meth:`~netsquid.qubits.ketstates.BellIndex.PHI_PLUS` Bell state directly after emission."""
    emission_fidelity_B: float = 1
    """Fidelity of state shared between photon and memory qubit on "B" side to
    :meth:`~netsquid.qubits.ketstates.BellIndex.PHI_PLUS` Bell state directly after emission."""
    coin_prob_ph_ph: float = 1
    """Coincidence probability for two photons. When using a coincidence time window in the double-click protocol,
    two clicks are only accepted if they occurred within one coincidence time window away from each other.
    This parameter is the probability that if both clicks are photon detections,
    they are within one coincidence window. In general, this depends not only on the size of the coincidence
    time window, but also on the state of emitted photons and the total detection time window."""
    coin_prob_ph_dc: float = 1
    """Coincidence probability for a photon and a dark count.
    When using a coincidence time window in the double-click protocol,
    two clicks are only accepted if they occurred within one coincidence time window away from each other.
    This parameter is the probability that if one click is a photon detection and the other a dark count,
    they are within one coincidence window. In general, this depends not only on the size of the coincidence
    time window, but also on the state of emitted photons and the total detection time window."""
    coin_prob_dc_dc: float = 1
    """Coincidence probability for two dark counts. When using a coincidence time window in the double-click protocol,
    two clicks are only accepted if they occurred within one coincidence time window away from each other.
    This parameter is the probability that if both clicks are dark counts,
    they are within one coincidence window. In general, this depends on the size of the coincidence time window
    and the total detection time window."""

    def verify(self):
        super().verify()
        self.verify_between_0_and_1("detector_efficiency")
        self.verify_between_0_and_1("dark_count_probability")
        self.verify_between_0_and_1("visibility")
        self.verify_is_type("num_resolving", bool)
        self.verify_not_negative_value("num_multiplexing_modes")
        self.verify_is_type("num_multiplexing_modes", int)
        self.verify_between_0_and_1("emission_fidelity_A")
        self.verify_between_0_and_1("emission_fidelity_B")
        self.verify_between_0_and_1("coin_prob_ph_ph")
        self.verify_between_0_and_1("coin_prob_ph_dc")
        self.verify_between_0_and_1("coin_prob_dc_dc")
