from __future__ import annotations

from dataclasses import dataclass, field
from typing import List
from abc import ABCMeta, abstractmethod

from netsquid_magic.magic_distributor import _EntGenRequest, MagicDistributor


class IQLink(metaclass=ABCMeta):
    @abstractmethod
    def open(self):
        """QLink is set to active, it becomes possible to generate entanglement via this qlink.
        Requests that were paused will start again."""
        pass

    @abstractmethod
    def close(self):
        """QLink is set to inactive. It is temporarily impossible to generate entanglement via this qlink.
        Requests in process are paused."""
        pass

    @property
    @abstractmethod
    def num_requests_in_queue(self) -> int:
        pass


@dataclass
class _EntDeliveryRequest:
    """Parameters to start a delivery using a MagicDistributor."""
    memory_positions: field(default_factory=dict)
    delivery_params: field(default_factory=dict)


class MagicQLink(IQLink):
    def __init__(self, magic_distributor: MagicDistributor):
        self.magic_distributor = magic_distributor
        self._is_open = True
        self._pending_pair_requests: List[_EntGenRequest] = []
        self._paused_requests: List[_EntDeliveryRequest] = []
        self.magic_distributor.add_callback(self._handle_state_delivery, event_type="state_delivery")
        self.magic_distributor.add_callback(self._handle_label_delivery, event_type="label_delivery")
        self._callbacks_state_delivery = []
        self._callbacks_label_delivery = []
        self.scheduler = None

    def open(self):
        self._is_open = True
        while len(self._paused_requests):
            req = self._paused_requests.pop()
            self.magic_distributor.add_delivery(memory_positions=req.memory_positions, **req.delivery_params)

    def close(self):
        self._is_open = False
        aborted_deliveries = self.magic_distributor.abort_all_delivery()

        for aborted_delivery in aborted_deliveries:
            if not self._delivery_mem_position_identical(aborted_delivery.delivery):
                self._paused_requests.append(_EntDeliveryRequest(memory_positions=aborted_delivery.delivery.memory_positions,
                                                                 delivery_params=aborted_delivery.delivery.parameters))

    def _delivery_mem_position_identical(self, delivery):
        for paused_del in self._paused_requests:
            if paused_del.memory_positions == delivery.memory_positions:
                return True
        return False

    def abort(self):
        self.magic_distributor.abort_all_delivery()

    def add_pair_request(
        self, senderID: int, remoteID: int, pos_sender, delivery_params=None
    ):
        request = _EntGenRequest(senderID=senderID,
                                 remoteID=remoteID,
                                 pos_sender=pos_sender,
                                 delivery_params=delivery_params)

        # check if request matches other request:
        matching_request = self._matches_existing_request(request)
        # ...and act accordingly
        if matching_request is None:
            self._pending_pair_requests.append(request)
        else:
            # delete the request from the list of pending requests
            self._pending_pair_requests.remove(matching_request)

            # ensure the magic distributor performs the delivery
            self._add_pair_delivery(request, matching_request)

    def _add_pair_delivery(self, request_a: _EntGenRequest, request_b: _EntGenRequest):
        """Function that combines information from both requests and calls :meth:`add_delivery`."""
        # determine the correct ordering of memory positions
        memory_positions = {r.senderID: r.pos_sender for r in [request_a, request_b]}

        # NOTE: currently only works when both requests have same parameters
        if request_a.delivery_params != request_b.delivery_params:
            raise ValueError("Different requests for magic must pass same parameters.")
        delivery_params = request_a.delivery_params

        if self._is_open:
            self.magic_distributor.add_delivery(memory_positions=memory_positions, **delivery_params)
        else:
            self._paused_requests.append(_EntDeliveryRequest(memory_positions, delivery_params))

    def _matches_existing_request(self, request):
        """Function that determines whether there is an existing matching request and returns it if yes."""
        for other_request in self._pending_pair_requests:
            if request.matches(other_request):
                return other_request
        return None

    def _handle_label_delivery(self, event):
        assert self._is_open
        for callback in self._callbacks_label_delivery:
            callback(event=event)

    def _handle_state_delivery(self, event):
        assert self._is_open
        for callback in self._callbacks_state_delivery:
            callback(event=event)

    def add_callback(self, callback, event_type="state_delivery"):
        if event_type == "state_delivery":
            self._callbacks_state_delivery.append(callback)
        elif event_type == "label_delivery":
            self._callbacks_label_delivery.append(callback)
        else:
            raise KeyError(f"Incorrect event type specified: {event_type}")

    def peek_node_delivery(self, event, allow_archive=False):
        return self.magic_distributor.peek_node_delivery(event, allow_archive)

    def get_bell_state(self):
        midpoint_outcome = self.magic_distributor.get_label(None)
        return self.magic_distributor.get_bell_state(midpoint_outcome=midpoint_outcome)

    @property
    def num_requests_in_queue(self):
        return len(self._paused_requests) + len(self._pending_pair_requests)
