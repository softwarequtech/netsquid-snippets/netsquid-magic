from typing import Dict
import logging

from netsquid import BellIndex
from netsquid_driver.EGP import EGPService
from netsquid_magic.link_layer import (
    MagicLinkLayerProtocolWithSignaling,
)
from qlink_interface import (
    ReqCreateBase,
    ResError,
)
from qlink_interface.interface import ResCreate


logger = logging.getLogger(__name__)


class EgpProtocol(EGPService):
    def __init__(
        self,
        node,
        magic_link_layer_protocol: MagicLinkLayerProtocolWithSignaling,
        name=None,
    ):
        super().__init__(node=node, name=name)
        self._ll_prot: MagicLinkLayerProtocolWithSignaling = magic_link_layer_protocol
        self._create_id_to_request: Dict[int, ReqCreateBase] = {}
        self.logger = logger.getChild(node.name)

    def run(self):
        while True:
            yield self.await_signal(
                sender=self._ll_prot,
                signal_label="react_to_{}".format(self.node.ID),
            )
            result = self._ll_prot.get_signal_result(
                label="react_to_{}".format(self.node.ID), receiver=self
            )
            if result.node_id == self.node.ID:
                if isinstance(result.msg, ResError):
                    self._handle_error(result.msg)
                else:
                    try:
                        BellIndex(result.msg.bell_state)
                    except AttributeError:
                        pass
                    except ValueError:
                        raise TypeError(
                            f"{result.msg.bell_state}, which was obtained from magic link layer protocol,"
                            f"is not a :class:`netsquid.qubits.ketstates.BellIndex`."
                        )
                    self.send_response(response=result.msg)
                    if isinstance(result, ResCreate):
                        self._create_id_to_request.pop(result.create_id)
                    if self._ll_prot.scheduler:
                        self._ll_prot.scheduler.register_result(
                            self.node.ID, result.msg
                        )

    def create_and_keep(self, req) -> int:
        super().create_and_keep(req)
        create_id = self._ll_prot.put_from(self.node.ID, req)
        if self._ll_prot.scheduler:
            self._ll_prot.scheduler.register_request(self.node.ID, req, create_id)
        self._create_id_to_request[create_id] = req
        return create_id

    def measure_directly(self, req) -> int:
        super().measure_directly(req)
        create_id = self._ll_prot.put_from(self.node.ID, req)
        if self._ll_prot.scheduler:
            self._ll_prot.scheduler.register_request(self.node.ID, req, create_id)
        self._create_id_to_request[create_id] = req
        return create_id

    def remote_state_preparation(self, req) -> int:
        super().remote_state_preparation(req)
        create_id = self._ll_prot.put_from(self.node.ID, req)
        return create_id

    def receive(self, req):
        super().receive(req)
        self._ll_prot.put_from(self.node.ID, req)

    def stop_receive(self, req):
        super().stop_receive(req)
        self._ll_prot.put_from(self.node.ID, req)

    def _handle_error(self, error: ResError):
        create_id = error.create_id
        if error.error_code == error.error_code.TIMEOUT:

            self.logger.info(
                f"Request to create entanglement id:{create_id})"
                f" from {self.node.name} was timed out, restarting"
            )
            req = self._create_id_to_request[create_id]
            new_create_id = self._ll_prot.put_from(self.node.ID, req)
            # TODO must remove old request to avoid memory build up, but get errors if I do that
            # self._create_id_to_request.pop(create_id)
            self._create_id_to_request[new_create_id] = req
            if self._ll_prot.scheduler:
                self._ll_prot.scheduler.register_error(self.node.ID, error)

            if self._ll_prot.scheduler:
                self._ll_prot.scheduler.register_request(
                    self.node.ID, req, new_create_id
                )
        else:
            self.logger.error(
                f"Unhandled error {error.error_code}. id:{create_id})"
                f" from {self.node.name}"
            )
