import itertools
import logging
import warnings
from dataclasses import dataclass
from typing import List, Dict, Iterable

from copy import copy, deepcopy

from netsquid.qubits.qubitapi import Qubit
from netsquid import BellIndex
import netsquid as ns
from netsquid.nodes.node import Node
from netsquid.qubits.qubitapi import create_qubits, assign_qstate

import pydynaa

from netsquid_magic.long_distance_interface import ILongDistanceInterface
from netsquid_magic.state_delivery_sampler import (
    PerfectStateSamplerFactory,
    SingleClickDeliverySamplerFactory,
    DoubleClickDeliverySamplerFactory,
    DepolariseStateSamplerFactory,
    DepolariseWithFailureStateSamplerFactory,
    BitflipStateSamplerFactory, DeliverySample,
)
from netsquid_magic.model_parameters import DepolariseModelParameters, \
    SingleClickModelParameters, DoubleClickModelParameters, IModelParameters, PerfectModelParameters, \
    BitFlipModelParameters, HeraldedModelParameters

logger = logging.getLogger(__name__)


@dataclass
class Delivery:
    sample: DeliverySample
    memory_positions: Dict[int, int]
    parameters: dict


@dataclass
class NodeDelivery:
    """Holds information about magic delivery at a specific node."""
    delivery: Delivery
    node_id: int
    qubits: List[Qubit]  # keys: int (node IDs), values: qubits to be delivered at node.


class MagicDistributor(pydynaa.Entity):
    """
    Distributes states into memories and connects two or more nodes.

    It now supports:
    (1) adding delivery which is called by only one of the nodes (done through :meth:`add_delivery`),
    (2) adding delivery only after both involved nodes filed the request for creating bipartite two-qubit entanglement
        (request filed through :meth:`add_pair_request`), and
    (3) merging two Magic Distributors together (this can be chained to achieve merging of more MDs).

    Adding delivery
    ---------------

    Either make use of :meth:`add_delivery` if only of of the nodes calls the MagicDistributor at a time,
    or use :meth:`add_pair_request` to submit a request for bipartite two-qubit entanglement.
    Note that the method `add_pair_request` currently supports only two-qubit entaglement. The Magic Distributor waits
    for request from both involved nodes and then calls `add_delivery` once.

    Example usage for adding delivery:

    Say there is a (very simplified) Magic Distributor `md` setup with a placeholder delivery_sampler_factory
    and two nodes A and B. It will be used to show two different approaches to adding delivery.

    .. code-block:: python

        nodeA = Node("nodeA", 0, qmemory=QuantumMemory(name="A", num_positions=20), port_names=["ENT_LABEL"])
        nodeB = Node("nodeB", 1, qmemory=QuantumMemory(name="B", num_positions=20), port_names=["ENT_LABEL"])

        md = MagicDistributor(delivery_sampler_factory=FakeDeliverySamplerFactory, nodes=[nodeA, nodeB])

        # This is how memory positions could look, i.e. the dictionary would be {0: 2, 1: 3}
        memory_positions = {nodeA.ID: 2, nodeB.ID: 3}
        parameters = {"cycle_time": 42}

        # Adding delivery by only one node
        md.add_delivery(memory_positions=memory_positions, **parameters)

        # The same delivery (line 57) would be achieved by the following lines
        md.add_pair_request(senderID=0, remoteID=1, pos_sender=2, delivery_params=parameters)  # request from A
        md.add_pair_request(senderID=1, remoteID=0, pos_sender=3, delivery_params=parameters)  # request from B

        # since now both nodes A and B submitted the request, `add_delivery` with the same parameters as in line 57
        # would be called internally

    Merging functionality
    ---------------------

    Two magic distributors can be merged with :meth:`merge_magic_distributor`. Even after merging, most information is
    kept separate (such that fixed delivery parameters can be different for original magic distributors). There is
    an "md_index" that refers to the position of the original MD within the chain and can be used to retrieve the
    relevant properties of magic distributor.

    Note: Currently, even if magic distributors are merged, :meth:`add_delivery` is supported only for nodes originating
    from the same magic distributor. (See the example usage for illustration of this.)

    Example usage for merging magic distributors:

    Say there are four nodes (nodeA, nodeB, nodeC, nodeD) that are initialised outside of this piece of example code.
    The node IDs are 0, 1, 2, 3 respectively.

    .. code-block:: python
        # create two magic distributors
        md = MagicDistributor(FakeDeliverySamplerFactoryA, [nodeA, nodeB])
        other_md = MagicDistributor(FakeDeliverySamplerFactoryB, [nodeC, nodeD])

        # merge magic distributor "other_md" into "md" (the "self" MD is the leftmost one in the created chain)
        md.merge_magic_distributor(other_md)

        # md now holds two delivery sampler factories and four nodes
        # notice the nested list of nodes which serves for knowing the length of original magic distributors
        print(md.delivery_sampler_factory)
        >> [FakeDeliverySamplerFactoryA, FakeDeliverySamplerFactoryB]
        print(md.nodes)
        >> [[nodeA, nodeB], [nodeC, nodeD]]

        # calling add delivery only works for either nodeA and nodeB, or nodeC and nodeD
        md.add_delivery({0: 1, 1: 2}, "cycle_time"=10)  # only calls delivery on nodeA and nodeB
    """

    STATE_DEL_EVTYPE = ns.EventType("STATE_DELIVERED", "State is put in memory")
    LABEL_DEL_EVTYPE = ns.EventType("LABEL_DELIVERED", "Label is put on port")

    def __init__(self, delivery_sampler_factory, model_params: IModelParameters, nodes, state_delay=0, label_delay=0,
                 num_qubits_per_memory=1, skip_rounds=True, **kwargs):
        """
        Magic distributor of states.

        Note that to allow merging of magic distributors, all variables are internally stored as lists or dictionaries.
        To see more of the reasoning behind chosen data structures, see :meth:`merge_magic_distributor`.

        Parameters
        ----------
        delivery_sampler_factory: :class:`~.state_delivery_sampler.IStateDeliverySamplerFactory`.
            The state delivery sampler factory
        model_params: IModelParameters
            Model parameters
        nodes : list of :obj:`~netsquid.nodes.node.Node`
            List of nodes for which entanglement can be produced.
        state_delay : dict or float or int (default 0)
            The times [ns] from the success until when the states should be put in the memory of each node.
            If a dict, keys should be node IDs (int) and items should be delays for respective nodes (int or float).
            If int or float, this is the state delay used for every node.
        label_delay : dict or float or int (default 0)
            The times [ns] from adding the state in memory until when the labels (measurement outcomes) should be put on
            the port of each node.
            If a dict, keys should be node IDs (int) and items should be delays for respective nodes (int or float).
            If int or float, this is the label delay used for every node.
        skip_rounds : bool
            If True, then only sample from states that are not None and skip rounds which failed.
            If False, also sample from None and schedule fails.
            Can be changed through :meth:`~.set_skip_rounds`
        """
        self.delivery_sampler_factory = [delivery_sampler_factory]
        # Make self.logger such that it logs as: netsquid_magic.magic_distributor.Alice-Bob
        logger_name = "-".join([node.name for node in nodes])
        self.logger = logger.getChild(logger_name)
        self._nodes = [nodes]
        self._qmemories = [self.get_qmemories_from_nodes()]

        # How long after the success to add the state to the memories
        if isinstance(state_delay, int) or isinstance(state_delay, float):
            state_delay = {node.ID: state_delay for node in nodes}
        elif not isinstance(state_delay, dict):
            raise TypeError("state_delay must be dict, int or float, not {}.".format(type(state_delay)))
        if not {node_id for node_id in state_delay} == {node.ID for node in nodes}:
            raise ValueError("state delay was not specified for all nodes.")
        self._state_delay = [state_delay]

        # How long after adding the state to memory to add the label to the port
        if isinstance(label_delay, int) or isinstance(label_delay, float):
            label_delay = {node.ID: label_delay for node in nodes}
        elif not isinstance(label_delay, dict):
            raise TypeError("label_delay must be dict, int or float, not {}.".format(type(label_delay)))
        if not {node_id for node_id in label_delay} == {node.ID for node in nodes}:
            raise ValueError("Label delay was not specified for all nodes.")
        self._label_delay = [label_delay]

        # Number of qubits that will be distributed per memory
        self._num_qubits_per_memory = [num_qubits_per_memory]

        self._current_delivery_id = -1

        # Check model parameters against constraints defined in model parameter class
        # and deepcopy to prevent side effects of user using model params for other purposes outside this scope
        model_params = deepcopy(model_params)
        model_params.verify()
        self._model_parameters: List[IModelParameters] = [model_params]

        # Set behaviour for skipping rounds when sampling
        self._skip_rounds = [skip_rounds]

        self._callbacks_state_delivery = [[]]  # every MD has a list of callbacks (Reacts on state delivery events)
        self._callbacks_label_delivery = [[]]  # same as callbacks, but reacts on label delivery events
        self._state_node_deliveries = {}  # keys = state events, values = DeliverySample objects
        self._label_node_deliveries = {}  # keys = label events, values = DeliverySample objects
        self._label_events_corresponding_to_state_events = {}
        self._evhandler = pydynaa.EventHandler(self._handle_node_delivery)
        self.start()

        # Used for the functionality of adding requests
        self._last_label = None
        self._pending_requests = []

        # Used for functionality of separate delivery for each node
        self._state_events_of_same_delivery = {}  # state event: set of all state events of same delivery

        # Archives for state/label deliveries. This keeps information available after a delivery has been handled.
        self._state_delivery_archive = {}
        self._label_delivery_archive = {}

    @property
    def long_distance_interface(self) -> ILongDistanceInterface:
        assert len(self.delivery_sampler_factory) == 1
        return self.delivery_sampler_factory[0].long_distance_interface

    @long_distance_interface.setter
    def long_distance_interface(self, value: ILongDistanceInterface):
        assert len(self.delivery_sampler_factory) == 1
        self.delivery_sampler_factory[0].long_distance_interface = value

    def get_qmemories_from_nodes(self):
        """
        Specifies the memories of the nodes in which the state will be magically put.

        Note: this method can be called only when the magic distributor has not yet been merged.

        Returns
        -------
        dict
            A dictionary of which the keys are the unique node ID's and the corresponding
            values are the memories in which the state should be put.
        """
        if len(self.nodes) > 1:
            raise ValueError("Function get_qmemories_from_nodes can only be called on MD that is not merged yet")

        qmemories = {}
        for node in list(itertools.chain(*self.nodes)):
            if isinstance(node, Node):
                qmemory = node.qmemory
                if qmemory is None:
                    raise ValueError("Node has no memory")
                qmemories[node.ID] = qmemory
            else:
                raise NotImplementedError
        return qmemories

    @property
    def nodes(self):
        """
        Nodes are kept in a list of lists.
        In case the magic distributor has not been merged, access the nodes through `self.nodes[0]`.
        """
        return self._nodes

    def start(self):
        self._wait(self._evhandler, entity=self, event_type=self.STATE_DEL_EVTYPE)
        self._wait(self._evhandler, entity=self, event_type=self.LABEL_DEL_EVTYPE)

    def stop(self):
        self._dismiss(self._evhandler, entity=self, event_type=self.STATE_DEL_EVTYPE)
        self._dismiss(self._evhandler, entity=self, event_type=self.LABEL_DEL_EVTYPE)
        if self._pending_requests:
            self.logger.warning(f"Unfinished pending requests: {self._pending_requests}")

    def reset(self):
        # reset pending requests and last label
        self._pending_requests = []
        self._last_label = None

        # reset the event handler
        self.stop()
        self.start()

    def add_delivery(self, memory_positions, md_index=0, **kwargs):
        """
        Called by only one of the nodes.

        Parameters
        ----------
        memory_positions : dict with keys = node IDs and values = memory positions
        md_index : int (default 0 for when magic distributor has not been merged yet)
            This index specifies at which positions the parameters for adding delivery are. In case of a merged MD,
            it is a position of the original Magic Distributor in the chain.
        kwargs : additional args that will be passed on to
            :obj:`~netsquid_magic.state_delivery_sampler.DeliverySampler`.sample

        Returns
        -------
        :obj:`~pydynaa.core.Event`
            One of the scheduled events (there is a different event for each node, but only one of them is returned).
            This event can be used subsequently in other methods such as :meth:`~MagicDistributor.peek_delivery`
            and :meth:`~MagicDistributor.abort_delivery`

        """
        # Check all nodes originate from the same magic distributor
        for nodeID in memory_positions.keys():
            if not any(node.ID == nodeID for node in self.nodes[md_index]):
                raise ValueError("Not all nodes refer to the same parameters, they have varying md_index")

        # Check that delivery is added to all quantum memories in the magic distributor
        if len(memory_positions) != len(self._qmemories[md_index]):
            raise ValueError("More memory positions_of_connections provided than QMemories found in the connection")

        for nodeID in memory_positions.keys():
            if isinstance(memory_positions[nodeID], int):
                # Wrap in a list in order to check its length and later assign the qstate consistently
                memory_positions[nodeID] = [memory_positions[nodeID]]
            if len(memory_positions[nodeID]) != self._num_qubits_per_memory[md_index]:
                raise ValueError("Number of given memory positions and specified number of qubits do not match")

        # add the delivery
        parameters = {}
        parameters.update(kwargs)
        parameters['md_index'] = md_index
        delivery_sampler = self.delivery_sampler_factory[md_index].create_state_delivery_sampler(model_params=self._model_parameters[md_index],
                                                                                                 **parameters)
        delivery = Delivery(
            sample=delivery_sampler.sample(skip_rounds=self._skip_rounds),
            memory_positions=memory_positions,
            parameters=parameters,
        )

        event = self._schedule_state_delivery_events(delivery)
        self._last_label = self.get_label(event)
        return event

    def get_label_delivery_event(self, event):
        """Get the corresponding label delivery event from a state delivery event.

        Parameters
        ----------
        event: `~pydynaa.core.Event`
            A state delivery event

        Returns
        -------
        :obj:`~pydynaa.core.Event`
            A label delivery event or None
        """
        if event in self._label_events_corresponding_to_state_events.keys():
            return self._label_events_corresponding_to_state_events[event]
        return None

    def add_pair_request(
        self, senderID, remoteID, pos_sender, delivery_params=None
    ):
        """
        Files a request to generate (bipartite) entanglement.
        Once both nodes have filed a request, :meth:`add_pair_delivery` is called to actually produce the entanglement.

        Parameters
        ----------
        senderID : int
            Node ID of the calling node.
        remoteID : int
            ID of the other node with whom entanglement should be generated.
        pos_sender : int
            Memory position of the calling node where the entanglement should be stored
        delivery_params : dict with keys = str and value = Any
            Parameters which are passed on to the `add_delivery` function of the MagicDistributor.
        """
        request = _EntGenRequest(senderID=senderID,
                                 remoteID=remoteID,
                                 pos_sender=pos_sender,
                                 delivery_params=delivery_params)

        # check if request matches other request:
        matching_request = self._matches_existing_request(request)
        # ...and act accordingly
        if matching_request is None:
            self._pending_requests.append(request)
        else:
            # delete the request from the list of pending requests
            self._pending_requests.remove(matching_request)

            # ensure the magic distributor performs the delivery
            event = self._add_pair_delivery(request, matching_request)
            self._last_label = self.get_label(event=event)

    def merge_magic_distributor(self, other):
        """
        Merges two magic distributors together. The data is combined into lists, lists of lists, or dictionaries as
        explained below.

        Note: merging magic distributors causes its history to be cleared. State and label deliveries data are kept,
        but the method :meth:`reset` is called which clears `_pending_requests` and resets `_last_label`. The
        `_current_delivery_id` is also reset as we don't know which MD has more recent information.

        Parameters
        ----------
        other : :obj:`~netsquid_magic.magic_distributor.MagicDistributor`

        """
        if not isinstance(other, MagicDistributor):
            raise TypeError("Parameter `other` should be a MagicDistributor")

        # one value per MD, thus kept in a list
        self.delivery_sampler_factory += other.delivery_sampler_factory
        self._state_delay += other._state_delay
        self._label_delay += other._label_delay
        self._num_qubits_per_memory += other._num_qubits_per_memory
        self._skip_rounds += other._skip_rounds

        # (possibly) list of values per MD, thus kept in a list of lists
        self._nodes += other.nodes
        self._callbacks_state_delivery += other._callbacks_state_delivery
        self._callbacks_label_delivery += other._callbacks_label_delivery

        # dictionaries with no duplicate keys (delivery has unique id in simulation run), dicts are updated
        self._state_node_deliveries.update(other._state_node_deliveries)
        self._label_node_deliveries.update(other._label_node_deliveries)

        # qmemories kept in list of dicts separate for easier checking of length of original magic distributors
        self._qmemories += other._qmemories

        # model parameters with possible duplicate values, thus kept in a list of parameters
        self._model_parameters += other._model_parameters

        self._current_delivery_id = -1  # Note: reset because we cannot know which MD has more recent information
        self.reset()

    def add_callback(self, callback, md_index=0, event_type="state_delivery"):
        """
        Parameters
        ----------
        callback : callback function with delivery_id as argument
        md_index : int (default 0 for when magic distributor has not been merged yet)
            Index of original magic distributor and its callbacks.
        event_type: str (default state_delivery)
            The type of event that triggers the callback. Possible values: "state_delivery" and "label_delivery".

        Notes
        -----
        Each callback gets called at every individual node delivery. Thus, for every time
        :meth:`~MagicDistributor.add_delivery` is called, each callback is called once for each node in
        :prop:`~MagicDistributor.nodes`.

        """
        if event_type == "state_delivery":
            self._callbacks_state_delivery[md_index].append(callback)
        elif event_type == "label_delivery":
            self._callbacks_label_delivery[md_index].append(callback)
        else:
            raise KeyError(f"Incorrect event type specified: {event_type}")

    def clear_all_callbacks(self):
        """Removes all set callback functions."""
        self._callbacks_state_delivery = [[]]
        self._callbacks_label_delivery = [[]]

    def set_skip_rounds(self, skip, md_index=0):
        """
        Function to change the behaviour of skipping rounds during sampling.

        Parameters
        ----------
        skip : Boolean
            If True, then only sample from states that are not None and skip rounds which failed.
            If False, also sample from None and schedule fails.
        md_index : int (default 0 for when magic distributor has not been merged yet)
            Index of original magic distributor and its skip_rounds property.
        """
        self._skip_rounds[md_index] = skip

    def get_label(self, event=None):
        """
        Function that either returns the last assigned label when successfully calling `add_pair_request`
        (when event is None) or a label of delivery of specified event.

        When no delivery is known corresponding to the specified event, None is returned.
        Note that this does not necessarily mean that there never was any delivery corresponding to that event,
        as deliveries are archived when completed and then later thrown out
        (currently only one delivery is kept in archive at a time).
        """
        if event is None:
            return self._last_label

        delivery = self.peek_delivery(event, allow_archive=True)
        if delivery is None:
            return None
        return delivery.sample.label

    def peek_node_delivery(self, event, allow_archive=False) -> NodeDelivery:
        """Returns node delivery of specified event without removing it from the dictionary.

        If allow_archive is true, the method also peeks in the delivery archive
        (currently only one old delivery is kept in archive at a time).
        """
        if event.type == self.STATE_DEL_EVTYPE:
            state_delivery = self._state_node_deliveries.get(event, None)
            if allow_archive and state_delivery is None:
                state_delivery = self._state_delivery_archive.get(event, None)
            return state_delivery
        elif event.type == self.LABEL_DEL_EVTYPE:
            label_delivery = self._label_node_deliveries.get(event, None)
            if allow_archive and label_delivery is None:
                label_delivery = self._label_delivery_archive.get(event, None)
            return label_delivery
        else:
            raise TypeError("Unknown event of type {}".format(event.type))

    def peek_delivery(self, event, allow_archive=False) -> Delivery:
        """Returns delivery of specified event without removing it from the dictionary.

        Returns None if no delivery is available.

        If allow_archive is true, the method also peeks in the delivery archive
        (currently only one old delivery is kept in archive at a time).
        """
        node_delivery = self.peek_node_delivery(event, allow_archive=allow_archive)
        delivery = node_delivery.delivery if node_delivery is not None else None
        return delivery

    def abort_delivery(self, event):
        """Removes delivery of specified event. If it is a state event, delivery of all state events (there is one
        event for each node) corresponding to the same delivery is removed. If it is a state event corresponding
        to a delivery for which the state has already been delivered but not the label, the delivery of the label
        is aborted. The memory positions associated with the delivery are freed."""
        if event.type == self.STATE_DEL_EVTYPE:
            if event in self._state_events_of_same_delivery:
                for event_of_same_delivery in self._state_events_of_same_delivery[event]:
                    if event_of_same_delivery in self._label_events_corresponding_to_state_events:
                        # if there was already a label delivery scheduled for this event
                        label_event = self._label_events_corresponding_to_state_events[event_of_same_delivery]
                        self._free_event_memory_positions(label_event)
                        self._pop_label_delivery(label_event)
                    if event_of_same_delivery != event:
                        self._free_event_memory_positions(event_of_same_delivery)
                        self._pop_state_delivery(event_of_same_delivery)
            self._free_event_memory_positions(event)
            return self._pop_state_delivery(event)
        elif event.type == self.LABEL_DEL_EVTYPE:
            self._free_event_memory_positions(event)
            return self._pop_label_delivery(event)
        else:
            raise TypeError("Unknown event of type {}".format(event.type))

    def _free_event_memory_positions(self, event):
        """Releases memory positions associated with the specified event's delivery."""
        delivery = self.peek_node_delivery(event)
        if delivery is None:
            return
        md_index = delivery.delivery.parameters["md_index"]
        for node_id, memory_positions in delivery.delivery.memory_positions.items():
            assert isinstance(memory_positions, Iterable)
            for memory_position in memory_positions:
                self._qmemories[md_index][node_id].mem_positions[memory_position].in_use = False

    def abort_all_delivery(self) -> List[NodeDelivery]:
        """Removes all deliveries and frees the associated memory positions."""
        aborted_deliveries = []
        state_event_list = copy(self._state_node_deliveries)
        for state_event in state_event_list:
            delivery = self.peek_node_delivery(state_event)
            aborted_deliveries.append(delivery)
            self._free_event_memory_positions(state_event)
            self._pop_state_delivery(state_event)

        label_event_list = copy(self._label_node_deliveries)
        for label_event in label_event_list:
            delivery = self.peek_node_delivery(label_event)
            self._free_event_memory_positions(label_event)
            aborted_deliveries.append(delivery)
            self._pop_label_delivery(label_event)
        self._pending_requests.clear()
        return aborted_deliveries

    def get_same_delivery_events(self, event) -> list:
        """
        Get all events related to the same delivery as this event or empty list if none are found.

        Parameters
        ----------
        event : :obj:`~pydynaa.core.Event`
            Representative event to get asocitated other events

        Returns
        -------
        list with entries :obj:`~pydynaa.core.Event`
            List of associated events
        """
        if event in self._state_events_of_same_delivery.keys():
            return self._state_events_of_same_delivery[event]
        return []

    def _handle_node_delivery(self, event):
        """Depending on the event (delivery of state or label), the state is put on memory or label is put on ports."""
        node_delivery = self.peek_node_delivery(event)
        if node_delivery is None:
            # delivery was aborted
            return
        if event.type == self.STATE_DEL_EVTYPE:
            self._handle_state_delivery(
                node_delivery=node_delivery,
                event=event,
            )

        elif event.type == self.LABEL_DEL_EVTYPE:
            self._handle_label_delivery(
                node_delivery=node_delivery,
                event=event,
            )

        else:
            raise TypeError("Unknown event type {}".format(event.type))

    def _handle_state_delivery(self, node_delivery, event):
        # let the calling node know that a state was created, only perform callbacks for that original MD
        delivery = node_delivery.delivery
        md_index = delivery.parameters['md_index']  # extract md_index from delivery information
        qmemory = self._qmemories[md_index][node_delivery.node_id]
        memory_positions = delivery.memory_positions[node_delivery.node_id]

        # deliver the quantum state at the quantum memory
        qmemory.put(qubits=node_delivery.qubits, positions=memory_positions, replace=True, check_positions=True)

        # apply noise to the delivered quantum state
        self._apply_noise(delivery=delivery, quantum_memory=qmemory, positions=list(range(qmemory.num_positions)))

        self._schedule_label_delivery_event(node_delivery=node_delivery, state_event=event)

        for callback in self._callbacks_state_delivery[delivery.parameters['md_index']]:
            callback(event=event)

        # ensure the same delivery will not be performed multiple times
        self._pop_state_delivery(event)

    def _handle_label_delivery(self, node_delivery, event):
        delivery = node_delivery.delivery

        for callback in self._callbacks_label_delivery[delivery.parameters["md_index"]]:
            callback(event=event)

        # ensure the same delivery will not be performed multiple times
        self._pop_label_delivery(event)

    def _matches_existing_request(self, request):
        """Function that determines whether there is an existing matching request and returns it if yes."""
        for other_request in self._pending_requests:
            if request.matches(other_request):
                return other_request
        return None

    def _add_pair_delivery(self, requestA, requestB):
        """Function that combines information from both requests and calls :meth:`add_delivery`."""
        # determine the correct ordering of memory positions
        memory_positions = {r.senderID: r.pos_sender for r in [requestA, requestB]}

        # NOTE: currently only works when both requests have same parameters
        if requestA.delivery_params != requestB.delivery_params:
            raise ValueError("Different requests for magic must pass same parameters.")
        delivery_params = requestA.delivery_params

        return self.add_delivery(memory_positions=memory_positions, **delivery_params)

    def _schedule_state_delivery_events(self, delivery):
        """Schedules the deliveries of the state and label."""
        md_index = delivery.parameters['md_index']  # extract md_index from delivery information
        state_events = []
        qubits = self._create_entangled_qubits(delivery)

        for node in self.nodes[md_index]:
            node_delivery = NodeDelivery(delivery=delivery, node_id=node.ID, qubits=qubits[node.ID])

            # set the memory positions at which a quantum state is delivered to "in use"
            for memory_position in delivery.memory_positions[node.ID]:
                self._qmemories[md_index][node.ID].mem_positions[memory_position].in_use = True

            # Schedule the delivery of the state
            # taking state delay of the original magic distributor this delivery applies to
            total_state_delay = self._get_total_state_delay(node_delivery=node_delivery)

            state_event = self._schedule_after(total_state_delay, self.STATE_DEL_EVTYPE)
            self._state_node_deliveries[state_event] = node_delivery
            state_events.append(state_event)

        # we choose one of the state events to use as reference for the delivery (does not really matter which one)
        state_event = state_events[0]
        # we store which other state events correspond to the same delivery as the reference state event
        self._state_events_of_same_delivery[state_event] = state_events

        return state_event

    def _get_total_state_delay(self, node_delivery):
        md_index = node_delivery.delivery.parameters['md_index']  # extract md_index from delivery information
        return node_delivery.delivery.sample.delivery_duration + self._state_delay[md_index][node_delivery.node_id]

    def _create_entangled_qubits(self, delivery):
        md_index = delivery.parameters['md_index']  # extract md_index from delivery information

        qubits = {}
        for node in self.nodes[md_index]:
            qubits[node.ID] = create_qubits(self._num_qubits_per_memory[md_index])

        # put the state onto the qubits
        ordered_qubits = []
        [ordered_qubits.extend(qubits[node.ID]) for node in self.nodes[md_index]]
        if delivery.sample.state is not None:
            assign_qstate(ordered_qubits, delivery.sample.state)

        # TODO: decide what to put on memory if failed / state=None
        return qubits

    def _schedule_label_delivery_event(self, node_delivery, state_event):
        """Schedules delivery of the label."""
        # taking label delay of the original magic distributor this delivery applies to
        md_index = node_delivery.delivery.parameters['md_index']  # extract md_index from delivery information
        label_delay = self._label_delay[md_index][node_delivery.node_id]
        label_event = self._schedule_after(label_delay, self.LABEL_DEL_EVTYPE)
        self._label_node_deliveries[label_event] = node_delivery
        self._label_events_corresponding_to_state_events[state_event] = label_event

        return label_event

    def _pop_state_delivery(self, event):
        """Removes state delivery event from dictionary.

        If a delivery is popped, it is also archived in case it is still required for future reference.
        Note: deliveries in archive can only be accessed using peek methods, not pop.
        """
        state_delivery = self._state_node_deliveries.pop(event, None)
        if state_delivery is not None:
            self._archive_state_delivery(event, state_delivery)
        return state_delivery

    def _archive_state_delivery(self, event, state_delivery):
        """Archive a state delivery so information about it can still be obtained in the future.

        Current implementation only keeps an archive of size one, but this could be extended if useful.
        Keeping an indefinite archive would probably mean you collect a lot of garbage though.
        """
        self._state_delivery_archive = {event: state_delivery}

    def _pop_label_delivery(self, event):
        """Removes label delivery event from dictionary.

        If a delivery is popped, it is also archived in case it is still required for future reference.
        Note: deliveries in archive can only be accessed using peek methods, not pop.
        """
        label_delivery = self._label_node_deliveries.pop(event, None)
        if label_delivery is not None:
            self._archive_label_delivery(event, label_delivery)
        for state_event, label_event in copy(self._label_events_corresponding_to_state_events).items():
            if label_event == state_event:
                del self._label_events_corresponding_to_state_events[state_event]

        return label_delivery

    def _archive_label_delivery(self, event, label_delivery):
        """Archive a label delivery so information about it can still be obtained in the future.

        Current implementation only keeps an archive of size one, but this could be extended if useful.
        Keeping an indefinite archive would probably mean you collect a lot of garbage though.
        """
        self._label_delivery_archive = {event: label_delivery}

    def _apply_noise(self, delivery, quantum_memory, positions):
        """
        Apply noise to e.g. carbon qubits.
        Meant to be overwritten.
        """
        pass

    def _get_node_from_id(self, node_id, md_index=0):
        """Get specific node given its ID and the position of its Magic Distributor.

        Parameters
        ----------
        node_id : int
            ID of the node that should be retrieved.
        md_index : int (default 0 for when magic distributor has not been merged yet)
            Index of relevant magic distributor which holds the node.
        """
        [node] = [n for n in self.nodes[md_index] if n.ID == node_id]
        return node

    def get_bell_state(self, midpoint_outcome):
        return BellIndex.PHI_PLUS


class PerfectStateMagicDistributor(MagicDistributor):
    def __init__(self, nodes, model_params: PerfectModelParameters, **kwargs):
        super().__init__(delivery_sampler_factory=PerfectStateSamplerFactory(),
                         model_params=model_params,
                         state_delay=model_params.state_delay,
                         label_delay=0,
                         nodes=nodes, **kwargs)


class DepolariseMagicDistributor(MagicDistributor):
    """
    Distributes (noisy) EPR pairs to 2 connected nodes, using samplers created
    by a :class:`~.state_delivery_sampler.DepolariseStateSamplerFactory`.
    """

    def __init__(self, nodes, model_params: DepolariseModelParameters, **kwargs):
        """
        Parameters
        ----------
        nodes : list of :obj:`~netsquid.nodes.node.Node`
            Pair of nodes to which noisy EPR pairs will be distributed.
        model_params : DepolariseModelParameters
            Model parameters for Depolarise model, but will not use `prob_success`.
        """
        super().__init__(delivery_sampler_factory=DepolariseStateSamplerFactory(),
                         model_params=model_params,
                         label_delay=0,
                         state_delay=model_params.cycle_time,
                         nodes=nodes, **kwargs)


class DepolariseWithFailureMagicDistributor(MagicDistributor):
    """
    Distributes (noisy) EPR pairs to 2 connected nodes, using samplers created
    by a :class:`~.state_delivery_sampler.DepolariseWithFailureStateSamplerFactory`.
    """

    def __init__(self, nodes, model_params: DepolariseModelParameters, **kwargs):
        """
        Parameters
        ----------
        nodes : list of :obj:`~netsquid.nodes.node.Node`
            Pair of nodes to which noisy EPR pairs will be distributed.
        model_params : DepolariseModelParameters
            Model parameters for Depolarise model.
        """
        super().__init__(delivery_sampler_factory=DepolariseWithFailureStateSamplerFactory(),
                         model_params=model_params,
                         label_delay=0,
                         state_delay=model_params.cycle_time,
                         nodes=nodes, **kwargs)

    def get_bell_state(self, midpoint_outcome) -> BellIndex:
        try:
            status, label = midpoint_outcome
        except ValueError:
            raise ValueError(f"Unknown midpoint outcome {midpoint_outcome}")
        assert status == 'success'
        assert isinstance(label, BellIndex)
        return label


class BitflipMagicDistributor(MagicDistributor):
    """
    Distributes (noisy) EPR pairs to 2 connected nodes, using samplers created
    by a :class:`~.state_delivery_sampler.BitflipStateSamplerFactory`.
    """

    def __init__(self, nodes, model_params: BitFlipModelParameters, **kwargs):
        """
        Parameters
        ----------
        nodes : list of :obj:`~netsquid.nodes.node.Node`
            Pair of nodes to which noisy EPR pairs will be distributed.
        model_params : BitFlipModelParameters
            Model parameters for Bit flip model.
        """
        super().__init__(delivery_sampler_factory=BitflipStateSamplerFactory(),
                         model_params=model_params, nodes=nodes, **kwargs)


class HeraldedConnectionMagicDistributor(MagicDistributor):
    """
    Magic distributor for nodes that are connected by a heralded connection.
    """
    def __init__(self, delivery_sampler_factory, model_params: HeraldedModelParameters, nodes: List[Node], **kwargs):

        node_ids = [node.ID for node in nodes]
        if len(node_ids) != 2:
            raise ValueError(f"{type(self)} expects exactly 2 nodes, got: {len(node_ids)}")

        model_params = deepcopy(model_params)
        model_params.verify_only_heralded_params()
        model_params.absorb_collection_efficiency_in_p_init()
        model_params, state_delays, label_delays = self._calculate_cycle_times_and_delays(model_params, node_ids)

        super().__init__(delivery_sampler_factory=delivery_sampler_factory,
                         model_params=model_params,
                         state_delay=state_delays,
                         label_delay=label_delays,
                         nodes=nodes, **kwargs)

    @staticmethod
    def _calculate_cycle_times_and_delays(model_params: HeraldedModelParameters, node_ids: List[int]):
        """Calculate parameters which are functions of other parameters.
        Should be called after all other parameters are set."""

        mp = model_params

        # calculate travel times
        travel_duration_A = mp.length_A / mp.speed_of_light_A * 1E9
        travel_duration_B = mp.length_B / mp.speed_of_light_B * 1E9

        # calculate cycle time (only if it was not specified explicitly in the init)
        if mp.cycle_time is None:
            cycle_time_A = 2 * travel_duration_A + mp.emission_duration_A
            cycle_time_B = 2 * travel_duration_B + mp.emission_duration_B
            mp.cycle_time = max(cycle_time_A, cycle_time_B)

        # update state and label delay
        time_of_BSM_A = mp.emission_duration_A + travel_duration_A
        time_of_BSM_B = mp.emission_duration_B + travel_duration_B
        time_of_BSM = max(time_of_BSM_A, time_of_BSM_B)

        state_delay_A = time_of_BSM - travel_duration_A
        state_delay_B = time_of_BSM - travel_duration_B

        label_delay_A = 2 * travel_duration_A
        label_delay_B = 2 * travel_duration_B

        state_delays = {node_ids[0]: state_delay_A, node_ids[1]: state_delay_B}
        label_delays = {node_ids[0]: label_delay_A, node_ids[1]: label_delay_B}

        return model_params, state_delays, label_delays


class SingleClickMagicDistributor(HeraldedConnectionMagicDistributor):
    """Magic distributor for when using a single-click protocol.

    Parameters
    ----------
    nodes : list of :obj:`~netsquid.nodes.node.Node`
        List of nodes for which entanglement can be produced.
        Node on "A" side of heralded connection should be listed first.
    model_params: SingleClickModelParameters
        Model parameters for single click model.

    Note
    ----
    Currently only works when the entanglement generation is completely symmetric between sides "A" and "B".

    """

    def __init__(self, nodes, model_params: SingleClickModelParameters, **kwargs):

        super().__init__(delivery_sampler_factory=SingleClickDeliverySamplerFactory(),
                         model_params=model_params,
                         nodes=nodes, **kwargs)

    def add_delivery(self, memory_positions, md_index=0, alpha=None, alpha_A=None, alpha_B=None, **kwargs):
        """
        Called by only one of the nodes. It is possible to set the bright state parameter uniformly for both nodes
        using `alpha`, or differently for the node on each side using `alpha_A` and `alpha_B`. Note that if both
        arguments are used, a warning will be raised and `alpha` will overwrite `alpha_A` and `alpha_B`.

        Parameters
        ----------
        memory_positions : dict
            Keys = node IDs and values = memory positions
        md_index : int (default 0 for when magic distributor has not been merged yet)
            This index specifies at which positions the parameters for adding delivery are. In case of a merged MD,
            it is a position of the original Magic Distributor in the chain.
        alpha : float or None (optional)
            Bright state parameter. Will be used for nodes on both sides of the connection.
        alpha_A : float or None (optional)
            Bright state parameter. Will be used for node on side "A" of the connection.
        alpha_B : float or None (optional)
            Bright state parameter. Will be used for node on side "B" of the connection.
        kwargs :
            additional args that will be passed on to
            :obj:`~netsquid_magic.state_delivery_sampler.DeliverySampler`.sample

        Returns
        -------
        :obj:`~pydynaa.core.Event`
            One of the scheduled events (there is a different event for each node, but only one of them is returned).
            This event can be used subsequently in other methods such as :meth:`~MagicDistributor.peek_delivery`
            and :meth:`~MagicDistributor.abort_delivery`
        """
        if alpha is not None and (alpha_A is not None or alpha_B is not None):
            warnings.warn(
                """You've request a delivery with a value for alpha but also for alpha_A and/or alpha_B.
                Did you mean to do this? The delivery will proceed using the value set for alpha.""")
        if alpha is not None:
            alpha_A = alpha
            alpha_B = alpha

        model_params = self._model_parameters[md_index]
        assert isinstance(model_params, SingleClickModelParameters)
        if model_params.bright_state_parameter_A:
            if alpha_A is not None:
                warnings.warn("Overwriting the value of alpha_A of the delivery with value "
                              "specified in model parameter bright_state_parameter_A")
            alpha_A = model_params.bright_state_parameter_A

        if model_params.bright_state_parameter_B:
            if alpha_B is not None:
                warnings.warn("Overwriting the value of alpha_B of the delivery with value "
                              "specified in model parameter bright_state_parameter_B")
            alpha_B = model_params.bright_state_parameter_B

        if alpha_A is None or alpha_B is None:
            raise ValueError("Single click model requires a bright state parameter value for A and B sides.")

        return super().add_delivery(memory_positions=memory_positions, md_index=md_index,
                                    alpha_A=alpha_A, alpha_B=alpha_B, **kwargs)

    def get_bell_state(self, midpoint_outcome) -> BellIndex:
        try:
            status, label = midpoint_outcome
        except ValueError:
            raise ValueError(f"Unknown midpoint outcome {midpoint_outcome}")
        assert status == 'success'
        assert isinstance(label, BellIndex)
        return label


class DoubleClickMagicDistributor(HeraldedConnectionMagicDistributor):
    """Magic distributor for when using a double-click protocol.

    Parameters
    ----------
    nodes : list of :obj:`~netsquid.nodes.node.Node`
        List of nodes for which entanglement can be produced.
        Node on "A" side of heralded connection should be listed first.
    model_params: DoubleClickModelParameters
        Model parameters for double click model.
    """

    def __init__(self, nodes, model_params: DoubleClickModelParameters, **kwargs):

        super().__init__(delivery_sampler_factory=DoubleClickDeliverySamplerFactory(),
                         model_params=model_params,
                         nodes=nodes, **kwargs)

    def get_bell_state(self, midpoint_outcome) -> BellIndex:
        try:
            status, label = midpoint_outcome
        except ValueError:
            raise ValueError(f"Unknown midpoint outcome {midpoint_outcome}")
        assert status == 'success'
        assert isinstance(label, BellIndex)
        return label


class _EntGenRequest:
    """
    Class that represents a "request" to generate entanglement by a node with another node;
    this class is only used internally by the :obj:`~netsquid_magic.magic_distributor.MagicDistributor`
    in order to find whether two nodes have both requested to generate entanglement with one another.
    """

    def __init__(self, senderID, remoteID, pos_sender, delivery_params=None):
        """
        Parameters
        ----------
        senderID : int
        remoteID : int
        pos_sender : int
        delivery_params : dict with keys=str and value=Any
        """
        assert (senderID != remoteID)
        self.senderID = senderID
        self.remoteID = remoteID
        self.pos_sender = pos_sender
        if delivery_params is None:
            self.delivery_params = {}
        else:
            self.delivery_params = delivery_params

    def __eq__(self, other):
        return (self.senderID == other.senderID and
                self.remoteID == other.remoteID and
                self.pos_sender == other.pos_sender)

    def __neq__(self, other):
        return not (self == other)

    def matches(self, other):
        """
        Checks whether there is a matching _EntGenRequest from the other node.

        Parameters
        ----------
        other: :obj:`~netsquid_magic.magic_distributor._EntGenRequest`
        """
        return self.senderID == other.remoteID and self.remoteID == other.senderID
