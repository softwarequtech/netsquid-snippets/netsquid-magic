API Documentation
-----------------

Below are the modules of this package.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/magic_distributor.rst
   modules/link_layer.rst
   modules/distributor_server.rst
   modules/state_delivery_sampler.rst
   modules/services.rst
   modules/magic.rst
   modules/model_parameters.rst

