model_parameters
-------------------

.. automodule:: netsquid_magic.model_parameters
    :members:
    :inherited-members:
    :show-inheritance:
