import netsquid as ns
from netsquid import sim_reset, sim_run, sim_time
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.nodes.node import Node
from qlink_interface import (
    ReqCreateAndKeep,
    ReqMeasureDirectly,
    ReqReceive,
    ResCreateAndKeep,
    ResMeasureDirectly,
)
from netsquid_magic.magic_distributor import SingleClickMagicDistributor
from netsquid_magic.model_parameters import SingleClickModelParameters
from netsquid_magic.link_layer import (
    LinkLayerService,
    MagicLinkLayerProtocol,
    SingleClickTranslationUnit,
)


def setup():
    ns.set_qstate_formalism(ns.QFormalism.DM)
    distance = 25  # length needs to be set for single-click MD when there is no heralded connection

    qmemA = QuantumProcessor("qmemA", 2)
    qmemB = QuantumProcessor("qmemA", 2)
    nodeA = Node("nodeA", 0, qmemory=qmemA)
    nodeB = Node("nodeB", 1, qmemory=qmemB)
    nodes = [nodeA, nodeB]

    model_params = SingleClickModelParameters(length_A=distance, length_B=distance)

    magic_distributor = SingleClickMagicDistributor(nodes, model_params=model_params)

    translation_unit = SingleClickTranslationUnit()

    magic_protocol = MagicLinkLayerProtocol(nodes=nodes, magic_distributor=magic_distributor, translation_unit=translation_unit)

    return nodeA, nodeB, magic_protocol


class User:
    def __init__(self, nodes):
        self.nodes = nodes
        self.create_ids = []

    def handle_msg(self, msg):
        print("Time {}: Received message {} from link layer service.".format(sim_time(), msg))

        if isinstance(msg, ResCreateAndKeep):
            # Get the state from the memory of one of the nodes
            qstate = self.nodes[0].qmemory.peek(0)[0].qstate
            print("Time {}: State of the entangled qubits is: \n{}".format(sim_time(), qstate))
        elif isinstance(msg, ResMeasureDirectly):
            print("Seq {}: Outcome/basis {}/{}".format(msg.sequence_number, msg.measurement_outcome, msg.measurement_basis))


def main():
    sim_reset()

    nodeA, nodeB, magic_protocol = setup()
    user = User(nodes=[nodeA, nodeB])
    service_interfaceA = LinkLayerService(nodeA, magic=True, magic_protocol=magic_protocol, reaction_handler=user.handle_msg)
    service_interfaceB = LinkLayerService(nodeB, magic=True, magic_protocol=magic_protocol, reaction_handler=user.handle_msg)

    service_interfaceA.start()
    service_interfaceB.start()

    # Install rule at B to be able to recv from A
    recv_request = ReqReceive(remote_node_id=nodeA.ID)
    service_interfaceB.put(recv_request)

    # NOTE For the linter
    _ = ReqCreateAndKeep
    _ = ReqMeasureDirectly

    create_request = ReqCreateAndKeep(remote_node_id=nodeB.ID, number=1, minimum_fidelity=0.9)
    # create_request = ReqMeasureDirectly(remote_node_id=nodeB.ID, number=10, minimum_fidelity=0.9)
    # create_request = ReqMeasureDirectly(remote_node_id=nodeB.ID, number=10, minimum_fidelity=0.9, x_rotation_angle_local_1=128)

    service_interfaceA.put(create_request)

    sim_run()


if __name__ == '__main__':
    main()
