from netsquid import sim_run
from example_service_protocol import setup_base_protocol, QuestionAnswerServiceProtocol, Question, Answer


class MagicQuestionAnswerProtocol(QuestionAnswerServiceProtocol):

    questions = ["qubits?"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._other_service = None

    def add_other_service(self, other_service):
        self._other_service = other_service

    def send_question(self, request):
        print(f"Magic protocol got request {request}")
        question = request.question
        if question not in self.questions:
            raise ValueError("Not a valid question")

        other_node = self._other_service.node

        other_num_qubits = other_node.nr_qubits
        response = Answer(other_num_qubits)
        print(f"Magic protocol sending response {response}")
        self.send_response(response)


def main():
    nodeA, nodeB, protoA, protoB = setup_base_protocol(MagicQuestionAnswerProtocol, magic=True)

    protoA.start()
    protoB.start()

    request = Question("qubits?")
    protoA.put(request)

    sim_run()


if __name__ == '__main__':
    main()
