import unittest

import netsquid as ns

from netsquid_magic.abstract_heralded_connection import AbstractHeraldedConnection, AbstractHeraldedMagic, \
    AbstractHeraldedModelParameters


class TestAbstractHeraldedConnection:

    @staticmethod
    def connection(length=100., fidelity=0.8, efficiency=0.1, num_modes=100, attenuation_coefficient=10.,
                   speed_of_light=300000.):
        return AbstractHeraldedConnection(name="test_abstract_heralded_connection",
                                          length=length, fidelity=fidelity, efficiency=efficiency, num_modes=num_modes,
                                          attenuation_coefficient=attenuation_coefficient,
                                          speed_of_light=speed_of_light)

    def test_prob_max_mixed(self):
        assert self.connection(fidelity=1.).prob_max_mixed == 0.
        assert self.connection(fidelity=.25).prob_max_mixed == 1.

    def test_success_probability(self):
        assert self.connection(length=0., efficiency=1., num_modes=1).success_probability == 1.
        assert self.connection(length=0., efficiency=1., num_modes=100).success_probability == 1.
        assert self.connection(length=0., efficiency=0.73, num_modes=1).success_probability == 0.73
        assert self.connection(length=0., efficiency=0.5, num_modes=2).success_probability == 3 / 4
        assert self.connection(length=0., efficiency=0.1, num_modes=10).success_probability > 0.1
        assert self.connection(length=10., attenuation_coefficient=1.,
                               num_modes=1, efficiency=1.).success_probability == 0.1

    def test_speed_of_light_delay(self):
        assert self.connection(length=0.).speed_of_light_delay == 0.
        assert self.connection(length=1., speed_of_light=1.).speed_of_light_delay == 1.E9  # 1 km in 1 second


class TestAbstractHeraldedMagic(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.prob_max_mixed = 0.3
        cls.success_probability = 0.1
        cls.speed_of_light_delay = 100
        cls.nodes = [ns.nodes.node.Node("Alice"), ns.nodes.node.Node("Bob")]
        for node in cls.nodes:
            node.qmemory = ns.components.QuantumMemory(name=f"test_memory_{node.name}")

    def _assert_parameters_correct(self, md):
        assert md._model_parameters[0].prob_max_mixed == self.prob_max_mixed
        assert md._model_parameters[0].prob_success == self.success_probability
        assert md._model_parameters[0].cycle_time == self.speed_of_light_delay

    def test_passing_parameters(self):
        model_params = AbstractHeraldedModelParameters(cycle_time=self.speed_of_light_delay,
                                                       prob_max_mixed=self.prob_max_mixed,
                                                       prob_success=self.success_probability,
                                                       )
        md = AbstractHeraldedMagic(nodes=self.nodes, model_params=model_params)
        self._assert_parameters_correct(md)

    def test_no_parameters(self):
        with self.assertRaises(AttributeError):
            AbstractHeraldedMagic(nodes=self.nodes, model_params=None)
