import dataclasses
import unittest
from unittest import mock

import netsquid as ns
import netsquid.qubits.qubitapi as qapi
import numpy as np
from netsquid import BellIndex
from netsquid.qubits import ketstates
from netsquid.qubits.state_sampler import StateSampler

from netsquid_magic.model_parameters import HeraldedModelParameters, SingleClickModelParameters, \
    DoubleClickModelParameters, DepolariseModelParameters, BitFlipModelParameters
from netsquid_magic.state_delivery_sampler import StateDeliverySampler, HeraldedStateDeliverySamplerFactory, \
    DoubleClickDeliverySamplerFactory, SingleClickDeliverySamplerFactory, DepolariseWithFailureStateSamplerFactory, \
    PerfectStateSamplerFactory, DepolariseStateSamplerFactory, BitflipStateSamplerFactory, \
    success_prob_and_fidelity_from_heralded_state_delivery_sampler_factory


class Test_StateDeliverySampler(unittest.TestCase):
    DUMMY_STATE_0 = np.array([1, 0])
    DUMMY_STATE_1 = np.array([0, 1])
    LABEL = 0

    def test_success_probability(self):
        random_ints_and_floats = [143, 42, 99.3, 12.5, 39.3]
        states = [self.DUMMY_STATE_0, None]
        for cycle_time in random_ints_and_floats:
            for (p, func_duration_check) in [
                    (1.0000, lambda duration: duration == 0),
                    (0.0001, lambda duration: duration > 0)]:
                state_sampler = StateSampler(qreprs=states,
                                             probabilities=[p, 1 - p],
                                             labels=[self.LABEL, None])
                gss = StateDeliverySampler(state_sampler=state_sampler,
                                           cycle_time=cycle_time)
                sampler, generation_duration, outcome = gss.sample()
                self.assertTrue(func_duration_check(generation_duration))
                self.assertTrue(isinstance(sampler.ket, np.ndarray))

    def test_error_handling_of_incorrect_input_parameters(self):
        CORRECT = 0
        INCORRECT = 1
        cycle_times = {CORRECT: [42., 12], INCORRECT: ["A", None, -5]}
        state_samplers = {CORRECT: [], INCORRECT: ["A", None, -5, 23.5]}

        # check that correct cases do not raise an error
        for cycle_time in cycle_times[CORRECT]:
            for state_sampler in state_samplers[CORRECT]:
                StateDeliverySampler(state_sampler=state_sampler, cycle_time=cycle_time)

        # check that incorrect cases raise an error
        for cycle_time in cycle_times[INCORRECT]:
            for state_sampler in state_samplers[INCORRECT]:
                with self.assertRaises(Exception):
                    StateDeliverySampler(state_sampler=state_sampler, cycle_time=cycle_time)


@dataclasses.dataclass
class TestHeraldedParams(HeraldedModelParameters):
    x: float = 0
    y: float = 0

    def verify(self):
        super().verify()
        self.verify_is_real_number("x")
        self.verify_is_real_number("y")


class TestHeraldedStateDeliverySamplerFactory(unittest.TestCase):

    @mock.patch.multiple(HeraldedStateDeliverySamplerFactory, __abstractmethods__=set())
    def test(self):
        def func_delivery(model_params: TestHeraldedParams, **kwargs):
            return StateSampler(qreprs=[stateA, stateB], probabilities=[model_params.x, model_params.y]), 1

        # Define input parameters
        stateA = np.array([[0.5, 0.5j],
                           [-0.5j, 0.5]])
        stateB = np.array([[0.5, 0],
                           [0, 0.5]])

        parameters = TestHeraldedParams(x=1., y=0., cycle_time=42.42)

        # Initialize the SamplerFactory
        tss_factory = HeraldedStateDeliverySamplerFactory(func_delivery=func_delivery)

        sampler = tss_factory.create_state_delivery_sampler(model_params=parameters)

        # Test the produced Sampler
        qstate, duration, outcome = sampler.sample()

        expected_state_sampler, __ = func_delivery(model_params=parameters)
        expected_leaves = expected_state_sampler.get_leaves()
        expected_dm = expected_leaves[0].qrepr.dm

        self.assertTrue(isinstance(qstate.dm, np.ndarray))
        self.assertTrue(isinstance(duration, float))
        self.assertEqual(duration, 0)

        self.assertTrue(np.allclose(qstate.dm, expected_dm))

    @mock.patch.multiple(HeraldedStateDeliverySamplerFactory, __abstractmethods__=set())
    def test_custom_labels(self):
        def func_delivery(model_params: TestHeraldedParams, **kwargs):
            return StateSampler(qreprs=[stateA, stateB], probabilities=[model_params.x, model_params.y],
                                labels=["Custom Label 1", "Custom Label 2"]), 1

        # Define input parameters
        stateA = np.array([[0.5, 0.5j],
                           [-0.5j, 0.5]])
        stateB = np.array([[0.5, 0],
                           [0, 0.5]])

        parameters = TestHeraldedParams(x=1., y=0., cycle_time=42.42)

        # Initialize the SamplerFactory
        tss_factory = HeraldedStateDeliverySamplerFactory(func_delivery=func_delivery)

        # Create Sampler with custom labels
        sampler = tss_factory.create_state_delivery_sampler(model_params=parameters)

        # Test the produced Sampler, output the labels
        _, _, outcome = sampler.sample()

        self.assertEqual(outcome[1], "Custom Label 1")

    @mock.patch.multiple(HeraldedStateDeliverySamplerFactory, __abstractmethods__=set())
    def test_custom_labels_wrong_length(self):
        def func_delivery(model_params: TestHeraldedParams, **kwargs):
            return StateSampler(qreprs=[stateA, stateB], probabilities=[model_params.x, model_params.y],
                                labels=["Custom Label 1", "Custom Label 2", "Extra custom label"]), 1

        # Define input parameters
        stateA = np.array([[0.5, 0.5j],
                           [-0.5j, 0.5]])
        stateB = np.array([[0.5, 0],
                           [0, 0.5]])

        parameters = TestHeraldedParams(x=1., y=0., cycle_time=42.42)

        # Initialize the SamplerFactory
        tss_factory = HeraldedStateDeliverySamplerFactory(func_delivery=func_delivery)

        # Try to create Sampler with incorrect amount of custom labels
        with self.assertRaises(ValueError):
            tss_factory.create_state_delivery_sampler(model_params=parameters)


class TestSingleClickDeliverySampler(unittest.TestCase):
    def get_model_params_perfect_values(self):
        params = SingleClickModelParameters()
        params.length_A = 0.
        params.length_B = 0.
        params.p_loss_length_A = 0.
        params.p_loss_length_B = 0.
        params.p_loss_init_A = 0.
        params.p_loss_init_B = 0.
        params.detector_efficiency = 1.
        return params

    alpha_A = 0.1
    alpha_B = 0.2

    @staticmethod
    def check_single_click_delivery_sampler_fidelity(state_sampler, fidelity):
        states = []
        for sample in state_sampler.get_leaves():
            states.append(sample.qrepr)

        for state in states:
            assert np.isclose(np.trace(state.dm), 1)
        ns.set_qstate_formalism(ns.QFormalism.DM)

        q1 = qapi.create_qubits(2, no_state=True)
        qapi.assign_qstate(q1, states[0])
        assert np.isclose(qapi.fidelity(q1, ketstates.b01, squared=True), fidelity)
        q2 = qapi.create_qubits(2, no_state=True)
        qapi.assign_qstate(q2, states[1])
        assert np.isclose(qapi.fidelity(q2, ketstates.b11, squared=True), fidelity)

    def test_perfect_values_not_number_resolving(self):
        """Assert expected results are obtained when single-click protocol is performed perfectly."""

        single_click_delivery_sampler_factory = SingleClickDeliverySamplerFactory()
        params = self.get_model_params_perfect_values()
        params.num_resolving = False

        state_sampler, success_prob = \
            single_click_delivery_sampler_factory._func_delivery(alpha_A=self.alpha_A, alpha_B=self.alpha_B,
                                                                 model_params=params)
        # Expected: success when one of the nodes emitted a photon
        # (probability: alpha_A * (1 - alpha_B) + alpha_B * (1 - alpha_A)), and success when both nodes emit a photon
        # (because the photons bunch) (probability alpha_A * alpha_B).
        case_1 = self.alpha_A * (1 - self.alpha_B) + self.alpha_B * (1 - self.alpha_A)
        case_2 = self.alpha_A * self.alpha_B
        expected_succ_prob = case_1 + case_2
        # fidelity contribution comes from up down and down up cases, which look like this if everything is perfect
        expected_fidelity_num = np.sqrt(self.alpha_A * self.alpha_B * (1 - self.alpha_A) * (1 - self.alpha_B)) + 0.5 * \
            (self.alpha_A * (1 - self.alpha_B) + self.alpha_B * (1 - self.alpha_A))

        # normalize dividing by the trace
        expected_fidelity_denom = self.alpha_A * self.alpha_B + self.alpha_A * (1 - self.alpha_B) + self.alpha_B * \
            (1 - self.alpha_A)
        expected_fidelity = expected_fidelity_num / expected_fidelity_denom
        self.check_single_click_delivery_sampler_fidelity(state_sampler, expected_fidelity)
        assert np.isclose(success_prob, expected_succ_prob)
        assert isinstance(state_sampler, StateSampler)

    def test_perfect_values_number_resolving(self):
        """Assert expected results are obtained when single-click protocol is performed perfectly."""

        single_click_delivery_sampler_factory = SingleClickDeliverySamplerFactory()

        params = self.get_model_params_perfect_values()
        params.num_resolving = True

        state_sampler, success_prob = \
            single_click_delivery_sampler_factory._func_delivery(alpha_A=self.alpha_A, alpha_B=self.alpha_B,
                                                                 model_params=params)
        # Expected: success only when one of the nodes emitted a photon
        # (probability alpha_A * (1 - alpha_B) + alpha_B * (1 - alpha_A)).
        # If both nodes emit a photon, it is detected that multiple photons arrive at the detector and failure
        # is heralded.
        # fidelity contribution comes from up down and down up cases, which look like this if everything is perfect
        expected_fidelity_num = np.sqrt(self.alpha_A * self.alpha_B * (1 - self.alpha_A) * (1 - self.alpha_B)) + 0.5 * \
            (self.alpha_A * (1 - self.alpha_B) + self.alpha_B * (1 - self.alpha_A))

        # normalize dividing by the trace. note one term missing when compared to non-number resolving situation, due to
        # up up situation not being falsely considered as a success here
        expected_fidelity_denom = self.alpha_A * (1 - self.alpha_B) + self.alpha_B * (1 - self.alpha_A)

        expected_fidelity = expected_fidelity_num / expected_fidelity_denom
        self.check_single_click_delivery_sampler_fidelity(state_sampler, expected_fidelity)
        assert np.isclose(success_prob, self.alpha_A * (1 - self.alpha_B) + self.alpha_B * (1 - self.alpha_A))
        assert isinstance(state_sampler, StateSampler)


class TestDoubleClickDeliverySampler(unittest.TestCase):
    def get_model_params_perfect_values(self):
        params = DoubleClickModelParameters()
        params.length_A = 0.
        params.length_B = 0.
        params.p_loss_length_A = 0.
        params.p_loss_length_B = 0.
        params.p_loss_init_A = 0.
        params.p_loss_init_B = 0.
        params.detector_efficiency = 1.
        params.dark_count_probability = 0
        params.detector_efficiency = 1
        params.num_multiplexing_modes = 1
        params.visibility = 1
        params.num_resolving = False,
        params.emission_fidelity_A = 1
        params.emission_fidelity_B = 1
        params.coin_prob_ph_ph = 1.
        params.coin_prob_ph_dc = 1.
        params.coin_prob_dc_dc = 1.
        return params

    @staticmethod
    def check_double_click_delivery_sampler_fidelity(state_sampler, fidelity):
        states = []
        for sample in state_sampler.get_leaves():
            states.append(sample.qrepr)

        if len(states) == 1:  # one test only has maximally mixed state as single leaf
            states = [states[0], states[0]]

        for state in states:
            assert np.isclose(np.trace(state.dm), 1)

        ns.set_qstate_formalism(ns.QFormalism.DM)

        q1 = qapi.create_qubits(2, no_state=True)
        qapi.assign_qstate(q1, states[0])
        assert np.isclose(qapi.fidelity(q1, ketstates.b01, squared=True), fidelity)
        q2 = qapi.create_qubits(2, no_state=True)
        qapi.assign_qstate(q2, states[1])
        assert np.isclose(qapi.fidelity(q2, ketstates.b11, squared=True), fidelity)

    def test_perfect_parameters(self):
        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()
        model_params = self.get_model_params_perfect_values()
        model_params.length_A = 5
        model_params.length_B = 10

        _, success_prob = double_click_delivery_sampler_factory. \
            _func_delivery(model_params=model_params)
        assert success_prob == .5  # maximum for linear-optical BSM
        probabilities = [double_click_delivery_sampler_factory._pt,
                         double_click_delivery_sampler_factory._pf1,
                         double_click_delivery_sampler_factory._pf2,
                         double_click_delivery_sampler_factory._pf3,
                         double_click_delivery_sampler_factory._pf4,
                         ]
        assert probabilities[0] == .5  # "real" success prob should be .5
        assert sum(probabilities) == .5  # "false" success probs should be 0
        state_s, _ = double_click_delivery_sampler_factory._func_delivery(model_params=model_params)
        self.check_double_click_delivery_sampler_fidelity(state_sampler=state_s, fidelity=1)

    def test_unit_dark_count_prob(self):
        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()
        model_params = self.get_model_params_perfect_values()
        model_params.dark_count_probability = 1

        with self.assertRaises(ValueError):
            _, _ = double_click_delivery_sampler_factory. \
                _func_delivery(model_params=model_params)
        probabilities = [double_click_delivery_sampler_factory._pt,
                         double_click_delivery_sampler_factory._pf1,
                         double_click_delivery_sampler_factory._pf2,
                         double_click_delivery_sampler_factory._pf3,
                         double_click_delivery_sampler_factory._pf4,
                         ]
        assert probabilities == [0, ] * 5
        assert double_click_delivery_sampler_factory.success_probability == 0

        # test error gets thrown again if the same call is made again;
        # is is relevant to test because usually the model is not recalculated if the same call is made multiple times
        with self.assertRaises(ValueError):
            _, _ = double_click_delivery_sampler_factory. \
                _func_delivery(model_params=model_params)

    def test_emission_fidelity(self):
        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()

        model_params = self.get_model_params_perfect_values()
        model_params.emission_fidelity_A = 0.5

        state_s, _ = double_click_delivery_sampler_factory. \
            _func_delivery(model_params=model_params)
        self.check_double_click_delivery_sampler_fidelity(state_sampler=state_s, fidelity=.5)

        model_params = self.get_model_params_perfect_values()
        model_params.emission_fidelity_A = 0.5
        model_params.emission_fidelity_B = 0.5

        state_s, _ = double_click_delivery_sampler_factory. \
            _func_delivery(model_params=model_params)
        self.check_double_click_delivery_sampler_fidelity(state_sampler=state_s, fidelity=1 / 3)

    def test_no_detector_efficiency(self):

        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()

        model_params = self.get_model_params_perfect_values()
        model_params.length_A = 5
        model_params.length_B = 10
        model_params.detector_efficiency = 0
        model_params.dark_count_probability = 0.5

        _, success_prob = double_click_delivery_sampler_factory. \
            _func_delivery(model_params=model_params)
        assert success_prob == 4 * .5 ** 4  # all click patterns have same probability, and 4 give success
        probabilities = [double_click_delivery_sampler_factory._pt,
                         double_click_delivery_sampler_factory._pf1,
                         double_click_delivery_sampler_factory._pf2,
                         double_click_delivery_sampler_factory._pf3,
                         double_click_delivery_sampler_factory._pf4,
                         ]
        assert probabilities == [0, 0, 0, 0, 4 * .5 ** 4]

        state_s, _ = double_click_delivery_sampler_factory._func_delivery(model_params=model_params)
        self.check_double_click_delivery_sampler_fidelity(state_sampler=state_s, fidelity=.25)  # maximally mixed state

        model_params = self.get_model_params_perfect_values()
        model_params.detector_efficiency = 0
        model_params.dark_count_probability = 0

        with self.assertRaises(ValueError):
            states, _ = double_click_delivery_sampler_factory._func_delivery(model_params=model_params)

    def test_multiplexing(self):
        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()

        model_params = self.get_model_params_perfect_values()
        model_params.length_A = 50
        model_params.length_B = 50
        model_params.num_multiplexing_modes = 10000
        assert double_click_delivery_sampler_factory._func_delivery(model_params=model_params)[1] > .9

        params_no_multiplexing = self.get_model_params_perfect_values()
        params_no_multiplexing.length_A = 50
        params_no_multiplexing.length_B = 50
        params_no_multiplexing.num_multiplexing_modes = 1

        params_with_multiplexing = self.get_model_params_perfect_values()
        params_with_multiplexing.length_A = 50
        params_with_multiplexing.length_B = 50
        params_with_multiplexing.num_multiplexing_modes = 10

        assert (double_click_delivery_sampler_factory._func_delivery(model_params=params_no_multiplexing)[1] <
                double_click_delivery_sampler_factory._func_delivery(model_params=params_with_multiplexing)[1])

        params_not_multiplexed = self.get_model_params_perfect_values()
        params_not_multiplexed.length_A = 5
        params_not_multiplexed.length_B = 5
        params_not_multiplexed.num_multiplexing_modes = 1

        states_not_multiplexed, prob_not_multiplexed = \
            double_click_delivery_sampler_factory._func_delivery(params_not_multiplexed)

        params_multiplexed = self.get_model_params_perfect_values()
        params_multiplexed.length_A = 5
        params_multiplexed.length_B = 5
        params_multiplexed.num_multiplexing_modes = 1000

        state_sampler_multiplexed, prob_multiplexed = \
            double_click_delivery_sampler_factory._func_delivery(params_multiplexed)
        for i in range(len(states_not_multiplexed.get_leaves())):
            assert state_sampler_multiplexed.get_leaves()[i].qrepr == states_not_multiplexed.get_leaves()[i].qrepr
            assert state_sampler_multiplexed.get_leaves()[i].probability == states_not_multiplexed.get_leaves()[i].probability
        assert prob_multiplexed != prob_not_multiplexed

    def test_zero_visibility(self):
        """Test if double-click model is correct when photons are fully distinguishable."""

        model_params = self.get_model_params_perfect_values()
        model_params.length_A = 5
        model_params.length_B = 5
        model_params.visibility = 0

        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()
        for num_resolving in [True, False]:
            model_params.num_resolving = num_resolving

            state_sampler, success_probability = \
                double_click_delivery_sampler_factory._func_delivery(model_params)
            assert success_probability == .5  # success if photons are detected in different bins, which is 50/50
            # should result in classically anticorrelated state, which has fidelity 0.5 to psi+ and psi-
            self.check_double_click_delivery_sampler_fidelity(state_sampler, .5)

    def test_num_resolving_without_dark_counts(self):
        """When there are no dark counts, it shouldn't matter whether detectors are photon-number-resolving or not."""
        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()
        model_params = self.get_model_params_perfect_values()
        model_params.length_A = 5
        model_params.length_B = 5
        model_params.p_loss_length_A = 0.1
        model_params.p_loss_length_B = 0.1
        model_params.p_loss_init_A = 0.1
        model_params.p_loss_init_B = 0.1
        model_params.emission_fidelity_A = 0.9
        model_params.emission_fidelity_B = 0.9
        model_params.detector_efficiency = 0.95
        model_params.visibility = 0.8
        model_params.num_resolving = True

        num_resolving_state_sampler, num_resolving_success_probability = \
            double_click_delivery_sampler_factory._func_delivery(model_params)

        model_params.num_resolving = False
        not_num_resolving_state_sampler, not_num_resolving_success_probability = \
            double_click_delivery_sampler_factory._func_delivery(model_params)
        assert np.isclose(num_resolving_success_probability, not_num_resolving_success_probability)

        fidelities = []
        for state_sampler in [num_resolving_state_sampler, not_num_resolving_state_sampler]:
            leaves = state_sampler.get_leaves()
            [psiminus_state] = [leave.qrepr for leave in leaves if leave.label is ketstates.BellIndex.B01]
            qubits = qapi.create_qubits(2)
            qapi.assign_qstate(qubits, psiminus_state)
            fidelities.append(qapi.fidelity(qubits, ketstates.b01, squared=True))
        num_resolving_fidelity = fidelities[0]
        not_num_resolving_fidelity = fidelities[1]
        assert np.isclose(num_resolving_fidelity, not_num_resolving_fidelity)

    def test_num_resolving_with_dark_counts(self):
        """When using photon-number-resolving detectors, dark counts can turn something that would have been a heralded
        success for non-photon-number-resolving detectors into a heralded failure.
        Therefore, the success probability should be smaller."""
        number_resolving = DoubleClickDeliverySamplerFactory()
        not_number_resolving = DoubleClickDeliverySamplerFactory()

        model_params = self.get_model_params_perfect_values()
        model_params.length_A = 5
        model_params.length_B = 5
        model_params.p_loss_length_A = 0.1
        model_params.p_loss_length_B = 0.1
        model_params.p_loss_init_A = 0.1
        model_params.p_loss_init_B = 0.1
        model_params.emission_fidelity_A = 0.9
        model_params.emission_fidelity_B = 0.9
        model_params.detector_efficiency = 0.95
        model_params.visibility = 0.8
        model_params.dark_count_probability = 0.1

        model_params.num_resolving = True
        _, number_resolving_success_probability = number_resolving. \
            _func_delivery(model_params)

        model_params.num_resolving = False
        _, not_number_resolving_success_probability = not_number_resolving. \
            _func_delivery(model_params)

        assert number_resolving_success_probability < not_number_resolving_success_probability

        # also check relation between the different detection probabilities
        assert number_resolving._pt < not_number_resolving._pt
        assert number_resolving._pf1 < not_number_resolving._pf1
        assert number_resolving._pf2 == 0
        assert not_number_resolving._pf2 > 0
        assert number_resolving._pf3 < not_number_resolving._pf3
        assert number_resolving._pf4 == not_number_resolving._pf4

    def test_only_ph_ph_coincidences_without_dark_counts(self, coin_prob_ph_ph=.4, visibility=.7):

        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()

        model_params = self.get_model_params_perfect_values()
        model_params.length_A = 5
        model_params.length_B = 5
        model_params.num_resolving = True
        model_params.visibility = visibility
        model_params.coin_prob_ph_ph = coin_prob_ph_ph
        model_params.coin_prob_ph_dc = 0.
        model_params.coin_prob_dc_dc = 0.

        state_sampler_no_dc, success_probability_no_dc = \
            double_click_delivery_sampler_factory._func_delivery(model_params)
        assert np.isclose(success_probability_no_dc, .5 * coin_prob_ph_ph)  # .5 is perfect value

        assert double_click_delivery_sampler_factory._pt == .5 * visibility * coin_prob_ph_ph
        assert double_click_delivery_sampler_factory._pf1 == .5 * (1 - visibility) * coin_prob_ph_ph

        expected_fidelity = visibility + (1 - visibility) / 2  # .5 fidelity when they don't interfere

        self.check_double_click_delivery_sampler_fidelity(state_sampler=state_sampler_no_dc, fidelity=expected_fidelity)

    def test_only_ph_ph_coincidences_with_dark_counts(self, coin_prob_ph_ph=.4, visibility=.7,
                                                      dark_count_probability=.6):
        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()

        model_params = self.get_model_params_perfect_values()
        model_params.length_A = 5
        model_params.length_B = 5
        model_params.num_resolving = True
        model_params.dark_count_probability = dark_count_probability
        model_params.visibility = visibility
        model_params.coin_prob_ph_ph = coin_prob_ph_ph
        model_params.coin_prob_ph_dc = 0.
        model_params.coin_prob_dc_dc = 0.

        state_sampler_with_dc, success_probability_with_dc = \
            double_click_delivery_sampler_factory._func_delivery(model_params)

        assert np.isclose(success_probability_with_dc, .5 * coin_prob_ph_ph * (1 - dark_count_probability) ** 4)

        # dark counts should not lower fidelity because there are no "success" events caused by dark counts
        expected_fidelity = visibility + (1 - visibility) / 2  # .5 fidelity when they don't interfere

        self.check_double_click_delivery_sampler_fidelity(state_sampler=state_sampler_with_dc,
                                                          fidelity=expected_fidelity)

        assert double_click_delivery_sampler_factory._pf2 == 0.
        assert double_click_delivery_sampler_factory._pf3 == 0.
        assert double_click_delivery_sampler_factory._pf4 == 0.

    def test_only_ph_dc_coincidences(self, coin_prob_ph_dc=.6, dark_count_probability=.4):

        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()

        model_params = self.get_model_params_perfect_values()
        model_params.length_A = 5
        model_params.length_B = 5
        model_params.p_loss_init_B = 1
        model_params.num_resolving = True
        model_params.dark_count_probability = dark_count_probability
        model_params.visibility = 0.8
        model_params.coin_prob_ph_ph = 0.
        model_params.coin_prob_ph_dc = coin_prob_ph_dc
        model_params.coin_prob_dc_dc = 0.

        state_sampler, success_probability = \
            double_click_delivery_sampler_factory._func_delivery(model_params)
        assert np.isclose(success_probability,
                          2 * dark_count_probability * (1 - dark_count_probability) ** 3 * coin_prob_ph_dc)

        assert double_click_delivery_sampler_factory._pt == 0.
        assert double_click_delivery_sampler_factory._pf1 == 0.
        assert double_click_delivery_sampler_factory._pf2 == 0.
        assert double_click_delivery_sampler_factory._pf4 == 0

        expected_fidelity = 1 / 4  # maximally-mixed state

        self.check_double_click_delivery_sampler_fidelity(state_sampler=state_sampler, fidelity=expected_fidelity)

    def test_only_dc_dc_coincidences(self, coin_prob_dc_dc=.3, dark_count_probability=.7):

        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()

        model_params = self.get_model_params_perfect_values()
        model_params.length_A = 5
        model_params.length_B = 5
        model_params.p_loss_init_A = 1
        model_params.p_loss_init_B = 1
        model_params.num_resolving = True
        model_params.dark_count_probability = dark_count_probability
        model_params.visibility = 0.8
        model_params.coin_prob_ph_ph = 0.
        model_params.coin_prob_ph_dc = 0.
        model_params.coin_prob_dc_dc = coin_prob_dc_dc

        state_sampler, success_probability = \
            double_click_delivery_sampler_factory._func_delivery(model_params)

        assert np.isclose(success_probability,
                          4 * dark_count_probability ** 2 * (1 - dark_count_probability) ** 2 * coin_prob_dc_dc)

        assert double_click_delivery_sampler_factory._pt == 0.
        assert double_click_delivery_sampler_factory._pf1 == 0.
        assert double_click_delivery_sampler_factory._pf2 == 0.
        assert double_click_delivery_sampler_factory._pf3 == 0

        expected_fidelity = 1 / 4  # maximally-mixed state

        self.check_double_click_delivery_sampler_fidelity(state_sampler=state_sampler, fidelity=expected_fidelity)

    def test_reusing_results(self):
        """If the same parameters are used as in previous run, results are recycled; check this is done correctly."""

        double_click_delivery_sampler_factory = DoubleClickDeliverySamplerFactory()
        model_params = self.get_model_params_perfect_values()
        model_params.length_A = 5
        model_params.length_B = 10

        state_sampler_1, success_prob_1 = double_click_delivery_sampler_factory. \
            _func_delivery(model_params)

        # calculate with same parameters again
        state_sampler_2, success_prob_2 = double_click_delivery_sampler_factory. \
            _func_delivery(model_params)

        # assert exactly the same objects are returned (i.e. the model is not reevaluated)
        assert state_sampler_1 is state_sampler_2
        assert success_prob_1 is success_prob_2

        # calculate with different parameters
        model_params.p_loss_init_A = 0.1
        state_sampler_3, success_prob_3 = double_click_delivery_sampler_factory. \
            _func_delivery(model_params)

        assert state_sampler_1 is not state_sampler_3
        assert success_prob_1 != success_prob_3


def test_depolarise_with_failure_state_sampler_factory(prob_max_mixed=.2, prob_success=.6):
    expected_fidelity = 1 - 3 * prob_max_mixed / 4
    depolarise_sampler_factory = DepolariseWithFailureStateSamplerFactory()

    model_params = DepolariseModelParameters()
    model_params.prob_max_mixed = prob_max_mixed
    model_params.prob_success = prob_success
    model_params.cycle_time = 1

    sampler, succ_prob = depolarise_sampler_factory._func_delivery(model_params)
    dm, _, label = sampler.get_leaves()[0]
    assert succ_prob == prob_success
    assert label is BellIndex.B00

    ns.set_qstate_formalism(ns.QFormalism.DM)
    q = qapi.create_qubits(2, no_state=True)
    qapi.assign_qstate(q, dm)
    assert np.isclose(qapi.fidelity(q, ketstates.b00, squared=True), expected_fidelity)


def test_depolarise_with_failure_state_sampler_factory_random_bell_index(prob_max_mixed=.2, prob_success=.6):
    expected_fidelity = 1 - 3 * prob_max_mixed / 4
    depolarise_sampler_factory = DepolariseWithFailureStateSamplerFactory()

    model_params = DepolariseModelParameters()
    model_params.prob_max_mixed = prob_max_mixed
    model_params.prob_success = prob_success
    model_params.cycle_time = 1
    model_params.random_bell_state = True

    sampler, succ_prob = depolarise_sampler_factory._func_delivery(model_params)

    assert succ_prob == prob_success

    labels = set()
    for dm, _, label in sampler.get_leaves():
        assert isinstance(label, BellIndex)

        ns.set_qstate_formalism(ns.QFormalism.DM)
        q = qapi.create_qubits(2, no_state=True)
        qapi.assign_qstate(q, dm)
        assert np.isclose(qapi.fidelity(q, ns.bell_states[label], squared=True), expected_fidelity)
        labels.add(label)

    assert len(labels) == 4


def test_perfect_state_sampler_factory():
    perfect_state_sampler_factory = PerfectStateSamplerFactory()
    sampler, succ_prob = perfect_state_sampler_factory._func_delivery()
    dm, _, label = sampler.get_leaves()[0]
    assert succ_prob == 1.
    assert label is BellIndex.B00
    ns.set_qstate_formalism(ns.QFormalism.DM)
    q = qapi.create_qubits(2, no_state=True)
    qapi.assign_qstate(q, dm)
    assert np.isclose(qapi.fidelity(q, ketstates.b00, squared=True), 1.)


def test_depolarise_state_sampler_factory(prob_max_mixed=.4):
    depolarise_sampler_factory = DepolariseStateSamplerFactory()

    model_params = DepolariseModelParameters()
    model_params.prob_max_mixed = prob_max_mixed
    model_params.cycle_time = 1

    sampler, succ_prob = depolarise_sampler_factory._func_delivery(model_params)
    (dm_perfect, p_perfect, label_perfect), (dm_depol, p_depol, label_depol) = sampler.get_leaves()
    assert succ_prob == 1.
    assert label_perfect is BellIndex.B00
    assert label_depol is BellIndex.B00
    assert p_depol == prob_max_mixed
    assert p_perfect == 1 - prob_max_mixed
    ns.set_qstate_formalism(ns.QFormalism.DM)
    q = qapi.create_qubits(2, no_state=True)
    qapi.assign_qstate(q, dm_perfect)
    assert np.isclose(qapi.fidelity(q, ketstates.b00, squared=True), 1.)
    q = qapi.create_qubits(2, no_state=True)
    qapi.assign_qstate(q, dm_depol)
    assert np.isclose(qapi.fidelity(q, ketstates.b00, squared=True), .25)


def test_bitflip_state_sampler_factory(flip_prob=.1):
    sampler_factory = BitflipStateSamplerFactory()
    model_params = BitFlipModelParameters(flip_prob=flip_prob)
    sampler, succ_prob = sampler_factory._func_delivery(model_params)
    (dm_perfect, p_perfect, label_perfect), (dm_flip, p_flip, label_flip) = sampler.get_leaves()
    assert succ_prob == 1.
    assert label_perfect is BellIndex.B00
    assert label_flip is BellIndex.B00
    assert p_flip == flip_prob
    assert p_perfect == 1 - flip_prob
    ns.set_qstate_formalism(ns.QFormalism.DM)
    q = qapi.create_qubits(2, no_state=True)
    qapi.assign_qstate(q, dm_perfect)
    assert np.isclose(qapi.fidelity(q, ketstates.b00, squared=True), 1.)
    q = qapi.create_qubits(2, no_state=True)
    qapi.assign_qstate(q, dm_flip)
    assert np.isclose(qapi.fidelity(q, ketstates.b01, squared=True), 1.)


def test_success_prob_and_fidelity_from_heralded_state_delivery_sampler_factory(error_prob=.3, success_prob=.1):
    p, f = success_prob_and_fidelity_from_heralded_state_delivery_sampler_factory(PerfectStateSamplerFactory)
    assert p == 1.
    assert np.isclose(f, 1.)
    model_params = BitFlipModelParameters(flip_prob=error_prob)
    p, f = success_prob_and_fidelity_from_heralded_state_delivery_sampler_factory(BitflipStateSamplerFactory,
                                                                                  model_params=model_params)
    assert p == 1.
    assert np.isclose(f, 1 - error_prob)
    model_params = DepolariseModelParameters(prob_max_mixed=error_prob)
    p, f = success_prob_and_fidelity_from_heralded_state_delivery_sampler_factory(DepolariseStateSamplerFactory,
                                                                                  model_params=model_params)
    assert p == 1.
    assert np.isclose(f, 1 - 3 * error_prob / 4)
    model_params = DepolariseModelParameters(prob_max_mixed=error_prob, prob_success=success_prob)
    p, f = success_prob_and_fidelity_from_heralded_state_delivery_sampler_factory(
        DepolariseWithFailureStateSamplerFactory, model_params=model_params)
    assert p == success_prob
    assert np.isclose(f, 1 - 3 * error_prob / 4)


if __name__ == "__main__":
    unittest.main()
