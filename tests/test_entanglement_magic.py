import unittest
from typing import List

import netsquid as ns
from netsquid.components.qmemory import QuantumMemory
from netsquid.nodes.node import Node
from netsquid.protocols.nodeprotocols import NodeProtocol
from netsquid_physlayer.classical_connection import ClassicalConnection

from netsquid_driver.classical_routing_service import ClassicalRoutingService
from netsquid_driver.driver import Driver
from netsquid_driver.entanglement_agreement_service import EntanglementAgreementService
from netsquid_driver.entanglement_service import ReqEntanglement, ReqEntanglementAbort, \
    ResEntanglementSuccess, ResEntanglementError, ResStateReady, EntanglementService
from netsquid_driver.initiative_based_agreement_service import InitiativeAgreementService, \
    InitiativeBasedAgreementServiceMode
from netsquid_magic.entanglement_magic import EntanglementMagic
from netsquid_magic.magic_distributor import PerfectStateMagicDistributor
from netsquid_magic.model_parameters import PerfectModelParameters
from netsquid_magic.qlink import MagicQLink


class EntanglementServiceListenerProtocol(NodeProtocol):
    """
    Protocol that listens to signals from the entanglement service and registers the event time and signal.

    :param node: Node this protocol is located on.
    :param entanglement_service: The entanglement service to listen to.
    """

    def __init__(self, node, entanglement_service: EntanglementService):
        super().__init__(node=node)
        self.received_state_ready = []
        self.received_success = []
        self.received_error = []
        self.entanglement_service = entanglement_service

    def run(self):
        evt_success = self.await_signal(
            sender=self.entanglement_service,
            signal_label=ResEntanglementSuccess.__name__,
        )
        evt_error = self.await_signal(
            sender=self.entanglement_service,
            signal_label=ResEntanglementError.__name__,
        )
        evt_state_ready = self.await_signal(
            sender=self.entanglement_service,
            signal_label=ResStateReady.__name__,
        )
        while True:
            evt_expr = yield evt_success | evt_error | evt_state_ready
            [evt] = evt_expr.triggered_events
            res = self.entanglement_service.get_signal_by_event(evt).result
            if isinstance(res, ResEntanglementSuccess):
                self.received_success.append((ns.sim_time(), res))
            elif isinstance(res, ResEntanglementError):
                self.received_error.append((ns.sim_time(), res))
            elif isinstance(res, ResStateReady):
                self.received_state_ready.append((ns.sim_time(), res))
            else:
                raise RuntimeError("Incorrect signal result")


class TestEntanglementMagic(unittest.TestCase):
    """
    Tests for EntanglementService.

    .. note::
        Expected times for request completion typically consist of twice classical message delay.
        This classical delay is due to the entanglement agreement service requiring
        two classical message interactions (ask and confirm) before entanglement generation can start.
    """

    def setUp(self) -> None:
        ns.sim_reset()

    @staticmethod
    def _create_star_network_with_services(center_node_name: str,
                                           spoke_node_names: List[str],
                                           classical_connection_delay: float = 0,
                                           quantum_entanglement_delay: float = 0,
                                           num_parallel_links: int = None,
                                           num_qubits=1) -> List[Node]:
        """
        Set up a star network with classical connections and perfect quantum links.
        The nodes will be set up with classical message routing, Initiative based agreement services
         and entanglement magic services.

        :param center_node_name: The name of the central node.
        :param spoke_node_names: A list of strings representing the names of the nodes in the network.
        :param classical_connection_delay: The classical message delay in nanoseconds for each connection
         in the network.
        :param quantum_entanglement_delay: The time it takes for entanglement to be generated.
        :param num_parallel_links: parameter for entanglement magic service.
         Determines how many active entanglement attempts can be active at the same time.
        :param num_qubits: Number of qubits on each of the qmemories at each node.
        :return: A list of Node objects with a driver and a ClassicalRoutingService.
        """
        # create center node
        center_node = Node(name=center_node_name)
        center_node.qmemory = QuantumMemory(num_positions=num_qubits, name="qmemory")
        center_node.add_ports(spoke_node_names)
        center_node.driver = Driver("driver")
        center_node.add_subcomponent(center_node.driver)

        # Create routing service for center node
        local_routing_table = {spoke_node_name: center_node.ports[spoke_node_name] for spoke_node_name in
                               spoke_node_names}
        routing_service = ClassicalRoutingService(center_node, local_routing_table)
        center_node.driver.add_service(ClassicalRoutingService, routing_service)

        # Create agreement service for center node
        agreement_service = InitiativeAgreementService(
            node=center_node,
            delay_per_node={spoke_node_name: classical_connection_delay for spoke_node_name in spoke_node_names},
            mode=InitiativeBasedAgreementServiceMode.InitiativeTaking
        )
        center_node.driver.add_service(EntanglementAgreementService, agreement_service)

        nodes = [center_node]
        center_node_link_dict = {}

        for spoke_node_name in spoke_node_names:
            # create spoke node
            node = Node(name=spoke_node_name)
            node.qmemory = QuantumMemory(num_positions=num_qubits, name="qmemory")
            node.add_ports([center_node_name])
            node.driver = Driver("driver")
            node.add_subcomponent(node.driver)
            nodes.append(node)

        # Create mapping between node name and node ID
        node_name_id_mapping = {node.name: node.ID for node in nodes}

        # loop over spoke nodes, excluding center node
        for node in nodes[1:]:
            spoke_node_name = node.name
            # create routing service for spoke node
            local_routing_table = {center_node_name: node.ports[center_node_name]}
            routing_service = ClassicalRoutingService(node, local_routing_table)
            node.driver.add_service(ClassicalRoutingService, routing_service)

            # create agreement service for spoke node
            agreement_service = InitiativeAgreementService(
                node=node,
                delay_per_node={center_node_name: classical_connection_delay},
                mode=InitiativeBasedAgreementServiceMode.Responding
            )
            node.driver.add_service(EntanglementAgreementService, agreement_service)

            # create classical connections
            connection = ClassicalConnection(f"connection_{center_node_name}_{spoke_node_name}",
                                             delay=classical_connection_delay)
            node.connect_to(remote_node=center_node,
                            connection=connection,
                            local_port_name=center_node_name, remote_port_name=spoke_node_name)

            # Create quantum connection
            magic_model_parameters = PerfectModelParameters()
            magic_model_parameters.state_delay = quantum_entanglement_delay
            magic_distributor = PerfectStateMagicDistributor(nodes=[node, center_node],
                                                             model_params=magic_model_parameters)
            magic_link = MagicQLink(magic_distributor)
            center_node_link_dict[node.name] = magic_link

            # Add entanglement service
            entanglement_service = EntanglementMagic(node,
                                                     qlink_dict={center_node_name: magic_link},
                                                     node_name_id_mapping=node_name_id_mapping)
            node.driver.add_service(EntanglementService, entanglement_service)

        # Add entanglement service center node
        entanglement_service = EntanglementMagic(center_node,
                                                 qlink_dict=center_node_link_dict,
                                                 node_name_id_mapping=node_name_id_mapping,
                                                 num_parallel_links=num_parallel_links)
        center_node.driver.add_service(EntanglementService, entanglement_service)

        return nodes

    def _check_memory_in_use(self, qmemory: QuantumMemory, expected_in_use_positions: List[int]):
        for i, mem_pos in enumerate(qmemory.mem_positions):
            if i in expected_in_use_positions:
                self.assertTrue(mem_pos.in_use)
            else:
                self.assertFalse(mem_pos.in_use)

    def test_two_node_network_single_request(self):
        """Check single request for entanglement will finish at the expected time"""
        classical_delay = 10
        quantum_delay = 50
        alice_node, bob_node = self._create_star_network_with_services(center_node_name="Alice",
                                                                       spoke_node_names=["Bob"],
                                                                       classical_connection_delay=classical_delay,
                                                                       quantum_entanglement_delay=quantum_delay,
                                                                       num_qubits=5)

        alice_entanglement_service = alice_node.driver.services[EntanglementService]
        bob_entanglement_service = bob_node.driver.services[EntanglementService]

        alice_receiver_protocol = EntanglementServiceListenerProtocol(alice_node, alice_entanglement_service)
        bob_receiver_protocol = EntanglementServiceListenerProtocol(bob_node, bob_entanglement_service)

        alice_node.driver.start_all_services()
        bob_node.driver.start_all_services()
        alice_receiver_protocol.start()
        bob_receiver_protocol.start()

        # check one entanglement
        alice_entanglement_service.put(ReqEntanglement("Bob", mem_pos=0))
        bob_entanglement_service.put(ReqEntanglement("Alice", mem_pos=0))

        ns.sim_run(duration=100)

        self._check_memory_in_use(alice_node.qmemory, [0])
        self._check_memory_in_use(bob_node.qmemory, [0])

        self.assertEqual(len(alice_receiver_protocol.received_success), 1)
        self.assertEqual(len(bob_receiver_protocol.received_success), 1)

        expected_time = 2 * classical_delay + quantum_delay
        self.assertAlmostEqual(alice_receiver_protocol.received_success[0][0], expected_time)
        self.assertAlmostEqual(bob_receiver_protocol.received_success[0][0], expected_time)

    def test_two_node_network_multiple_requests(self):
        """Check multiple request for entanglement will finish sequentially, each at the expected time"""
        classical_delay = 10
        quantum_delay = 50
        alice_node, bob_node = self._create_star_network_with_services(center_node_name="Alice",
                                                                       spoke_node_names=["Bob"],
                                                                       classical_connection_delay=classical_delay,
                                                                       quantum_entanglement_delay=quantum_delay,
                                                                       num_qubits=5)

        alice_entanglement_service = alice_node.driver.services[EntanglementService]
        bob_entanglement_service = bob_node.driver.services[EntanglementService]

        alice_receiver_protocol = EntanglementServiceListenerProtocol(alice_node, alice_entanglement_service)
        bob_receiver_protocol = EntanglementServiceListenerProtocol(bob_node, bob_entanglement_service)

        alice_node.driver.start_all_services()
        bob_node.driver.start_all_services()
        alice_receiver_protocol.start()
        bob_receiver_protocol.start()

        alice_entanglement_service.put(ReqEntanglement("Bob", mem_pos=0))
        bob_entanglement_service.put(ReqEntanglement("Alice", mem_pos=0))
        bob_entanglement_service.put(ReqEntanglement("Alice", mem_pos=1))

        ns.sim_run(duration=20)

        alice_entanglement_service.put(ReqEntanglement("Bob", mem_pos=1))
        alice_entanglement_service.put(ReqEntanglement("Bob", mem_pos=3))
        bob_entanglement_service.put(ReqEntanglement("Alice", mem_pos=3))

        ns.sim_run()

        self._check_memory_in_use(alice_node.qmemory, [0, 1, 3])
        self._check_memory_in_use(bob_node.qmemory, [0, 1, 3])

        self.assertEqual(len(alice_receiver_protocol.received_success), 3)
        self.assertEqual(len(bob_receiver_protocol.received_success), 3)

        expected_time = 2 * classical_delay + quantum_delay
        self.assertAlmostEqual(alice_receiver_protocol.received_success[0][0], expected_time)
        self.assertAlmostEqual(bob_receiver_protocol.received_success[0][0], expected_time)

        expected_time = expected_time + 2 * classical_delay + quantum_delay
        self.assertAlmostEqual(alice_receiver_protocol.received_success[1][0], expected_time)
        self.assertAlmostEqual(bob_receiver_protocol.received_success[1][0], expected_time)

        expected_time = expected_time + 2 * classical_delay + quantum_delay
        self.assertAlmostEqual(alice_receiver_protocol.received_success[2][0], expected_time)
        self.assertAlmostEqual(bob_receiver_protocol.received_success[2][0], expected_time)

    def test_two_node_network_unavailable_memory(self):
        """Check that an entanglement request for an unavailable memory,
         will pause and continue after the position becomes available"""
        classical_delay = 10
        quantum_delay = 50
        alice_node, bob_node = self._create_star_network_with_services(center_node_name="Alice",
                                                                       spoke_node_names=["Bob"],
                                                                       classical_connection_delay=classical_delay,
                                                                       quantum_entanglement_delay=quantum_delay,
                                                                       num_qubits=5)

        alice_entanglement_service = alice_node.driver.services[EntanglementService]
        bob_entanglement_service = bob_node.driver.services[EntanglementService]

        alice_receiver_protocol = EntanglementServiceListenerProtocol(alice_node, alice_entanglement_service)
        bob_receiver_protocol = EntanglementServiceListenerProtocol(bob_node, bob_entanglement_service)

        alice_node.driver.start_all_services()
        bob_node.driver.start_all_services()
        alice_receiver_protocol.start()
        bob_receiver_protocol.start()

        alice_node.qmemory.mem_positions[0].in_use = True

        alice_entanglement_service.put(ReqEntanglement("Bob", mem_pos=0))
        bob_entanglement_service.put(ReqEntanglement("Alice", mem_pos=0))

        blocked_memory_time = 1000
        ns.sim_run(blocked_memory_time)

        self.assertEqual(len(alice_receiver_protocol.received_success), 0)
        self.assertEqual(len(bob_receiver_protocol.received_success), 0)

        # TODO We have to use an internal entanglement_magic method that sends a signal
        #  (this only happens if they were used by another entanglement attempt, not for a quantum program.)
        #  Ideally a system would need to exist that can send this signal, like a memory manager
        alice_entanglement_service.set_in_use(0, False)

        ns.sim_run()

        self.assertEqual(len(alice_receiver_protocol.received_success), 1)
        self.assertEqual(len(bob_receiver_protocol.received_success), 1)

        expected_time = blocked_memory_time + 2 * classical_delay + quantum_delay
        self.assertAlmostEqual(alice_receiver_protocol.received_success[0][0], expected_time)
        self.assertAlmostEqual(bob_receiver_protocol.received_success[0][0], expected_time)

    def test_star_network_limited_parallel_entanglement(self):
        """Check that in a star network with limited capacity, entanglement requests will complete sequentially
        at the expected times. (The capacity is set to two, so: first two will complete, then the next two)"""
        classical_delay = 10
        quantum_delay = 50
        num_spokes = 4
        nodes = self._create_star_network_with_services(center_node_name="center",
                                                        spoke_node_names=[f"spoke_{i}" for i in range(num_spokes)],
                                                        classical_connection_delay=classical_delay,
                                                        quantum_entanglement_delay=quantum_delay,
                                                        num_qubits=10,
                                                        num_parallel_links=2)

        receiver_protocols = []
        entanglement_services = []

        for node in nodes:
            entanglement_service = node.driver.services[EntanglementService]
            receiver_protocol = EntanglementServiceListenerProtocol(node, entanglement_service)
            node.driver.start_all_services()
            receiver_protocol.start()
            receiver_protocols.append(receiver_protocol)
            entanglement_services.append(entanglement_service)

        center_node = nodes[0]
        spoke_nodes = nodes[1:]
        center_receiver_protocol = receiver_protocols[0]
        spoke_receiver_protocols = receiver_protocols[1:]
        center_entanglement_service = entanglement_services[0]
        spoke_entanglement_services = entanglement_services[1:]

        for i in range(num_spokes):
            center_entanglement_service.put(ReqEntanglement(spoke_nodes[i].name, mem_pos=i))
            spoke_entanglement_services[i].put(ReqEntanglement(center_node.name, mem_pos=0))

        ns.sim_run()

        self._check_memory_in_use(center_node.qmemory, [0, 1, 2, 3])
        for i in range(num_spokes):
            self._check_memory_in_use(spoke_nodes[i].qmemory, [0])

        self.assertEqual(len(center_receiver_protocol.received_success), 4)
        for i in range(num_spokes):
            self.assertEqual(len(spoke_receiver_protocols[i].received_success), 1)

        expected_time = 2 * classical_delay + quantum_delay
        self.assertAlmostEqual(center_receiver_protocol.received_success[0][0], expected_time)
        self.assertAlmostEqual(center_receiver_protocol.received_success[1][0], expected_time)
        self.assertAlmostEqual(spoke_receiver_protocols[0].received_success[0][0], expected_time)
        self.assertAlmostEqual(spoke_receiver_protocols[1].received_success[0][0], expected_time)

        expected_time = expected_time + 2 * classical_delay + quantum_delay
        self.assertAlmostEqual(center_receiver_protocol.received_success[2][0], expected_time)
        self.assertAlmostEqual(center_receiver_protocol.received_success[3][0], expected_time)
        self.assertAlmostEqual(spoke_receiver_protocols[2].received_success[0][0], expected_time)
        self.assertAlmostEqual(spoke_receiver_protocols[3].received_success[0][0], expected_time)

    def test_star_network_abort(self):
        """Check that in a star network an abort request will abort all requests to that node, but not others.
        (Two requests are generated for each node, so at the moment of abort, one is in active process,
         the other in queue. Both of these should be canceled.)"""
        classical_delay = 10
        quantum_delay = 50
        num_spokes = 4
        nodes = self._create_star_network_with_services(center_node_name="center",
                                                        spoke_node_names=[f"spoke_{i}" for i in range(num_spokes)],
                                                        classical_connection_delay=classical_delay,
                                                        quantum_entanglement_delay=quantum_delay,
                                                        num_qubits=10,
                                                        num_parallel_links=2)

        receiver_protocols = []
        entanglement_services = []

        for node in nodes:
            entanglement_service = node.driver.services[EntanglementService]
            receiver_protocol = EntanglementServiceListenerProtocol(node, entanglement_service)
            node.driver.start_all_services()
            receiver_protocol.start()
            receiver_protocols.append(receiver_protocol)
            entanglement_services.append(entanglement_service)

        center_node = nodes[0]
        spoke_nodes = nodes[1:]
        center_receiver_protocol = receiver_protocols[0]
        spoke_receiver_protocols = receiver_protocols[1:]
        center_entanglement_service = entanglement_services[0]
        spoke_entanglement_services = entanglement_services[1:]

        # check that abort will cancel all requests for that node
        for i in range(num_spokes):
            center_entanglement_service.put(ReqEntanglement(spoke_nodes[i].name, mem_pos=i))
            spoke_entanglement_services[i].put(ReqEntanglement(center_node.name, mem_pos=0))
        for i in range(num_spokes):
            center_entanglement_service.put(ReqEntanglement(spoke_nodes[i].name, mem_pos=num_spokes + i))
            spoke_entanglement_services[i].put(ReqEntanglement(center_node.name, mem_pos=1))

        ns.sim_run(duration=classical_delay)

        center_entanglement_service.put(ReqEntanglementAbort(spoke_nodes[0].name))
        center_entanglement_service.put(ReqEntanglementAbort(spoke_nodes[2].name))
        # TODO aborts need to be done on both sides, possibly change this to be one sided
        spoke_entanglement_services[0].put(ReqEntanglementAbort(center_node.name))
        spoke_entanglement_services[2].put(ReqEntanglementAbort(center_node.name))
        ns.sim_run()

        self._check_memory_in_use(center_node.qmemory, [1, 3, 5, 7])
        self._check_memory_in_use(spoke_nodes[0].qmemory, [])
        self._check_memory_in_use(spoke_nodes[1].qmemory, [0, 1])
        self._check_memory_in_use(spoke_nodes[2].qmemory, [])
        self._check_memory_in_use(spoke_nodes[3].qmemory, [0, 1])

        self.assertEqual(len(center_receiver_protocol.received_error), 2)
        self.assertEqual(len(spoke_receiver_protocols[0].received_error), 1)
        self.assertEqual(len(spoke_receiver_protocols[1].received_error), 0)
        self.assertEqual(len(spoke_receiver_protocols[2].received_error), 1)
        self.assertEqual(len(spoke_receiver_protocols[3].received_error), 0)

        self.assertEqual(len(spoke_receiver_protocols[1].received_success), 2)
        self.assertEqual(len(spoke_receiver_protocols[3].received_success), 2)

    def test_star_network_unlimited_parallel_entanglement(self):
        """Check that in a star network with unlimited capacity,
         entanglement requests will complete all at the same time."""
        classical_delay = 10
        quantum_delay = 50
        num_spokes = 4
        nodes = self._create_star_network_with_services(center_node_name="center",
                                                        spoke_node_names=[f"spoke_{i}" for i in range(num_spokes)],
                                                        classical_connection_delay=classical_delay,
                                                        quantum_entanglement_delay=quantum_delay,
                                                        num_qubits=5,
                                                        num_parallel_links=None)

        receiver_protocols = []
        entanglement_services = []

        for node in nodes:
            entanglement_service = node.driver.services[EntanglementService]
            receiver_protocol = EntanglementServiceListenerProtocol(node, entanglement_service)
            node.driver.start_all_services()
            receiver_protocol.start()
            receiver_protocols.append(receiver_protocol)
            entanglement_services.append(entanglement_service)

        center_node = nodes[0]
        spoke_nodes = nodes[1:]
        center_receiver_protocol = receiver_protocols[0]
        spoke_receiver_protocols = receiver_protocols[1:]
        center_entanglement_service = entanglement_services[0]
        spoke_entanglement_services = entanglement_services[1:]

        for i in range(num_spokes):
            center_entanglement_service.put(ReqEntanglement(spoke_nodes[i].name, mem_pos=i))
            spoke_entanglement_services[i].put(ReqEntanglement(center_node.name, mem_pos=0))

        ns.sim_run()

        self._check_memory_in_use(center_node.qmemory, [0, 1, 2, 3])
        for i in range(num_spokes):
            self._check_memory_in_use(spoke_nodes[i].qmemory, [0])

        self.assertEqual(len(center_receiver_protocol.received_success), 4)
        for i in range(num_spokes):
            self.assertEqual(len(spoke_receiver_protocols[i].received_success), 1)

        expected_time = 2 * classical_delay + quantum_delay
        for i in range(num_spokes):
            self.assertAlmostEqual(center_receiver_protocol.received_success[i][0], expected_time)
            self.assertAlmostEqual(spoke_receiver_protocols[i].received_success[0][0], expected_time)
