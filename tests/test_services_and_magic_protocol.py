import unittest
from collections import defaultdict

from netsquid.nodes.node import Node

from netsquid_magic.magic import MagicProtocol
from netsquid_magic.services import ServiceInterface, ServiceProtocol


# Faulty service protocol missing put method overload
class NoPutServiceProtocol(ServiceProtocol):
    pass


class CorrectServiceProtocol(ServiceProtocol):
    def __init__(self):
        super().__init__()
        self.requests = []

    def put(self, request):
        self.requests.append(request)


# Faulty service interface missing request_types and reaction_types methods overload
class NoTypeServiceInterface(ServiceInterface):
    pass


# Faulty service interface missing request_types method overload
class NoRequestTypeServiceInterface(ServiceInterface):
    reaction_types = [object]


# Faulty service interface missing reaction_types method overload
class NoReactionTypeServiceInterface(ServiceInterface):
    request_types = [object]


# Faulty service interface missing request_types and reaction_types methods overload
class CorrectServiceInterface(ServiceInterface):
    request_types = [int]
    reaction_types = [int]


class CorrectMagicProtocol(MagicProtocol):
    def __init__(self, nodes):
        super().__init__(nodes=nodes)
        self.requests = defaultdict(list)

    def put_from(self, node_id, request):
        self.requests[node_id].append(request)


class TestServiceProtocol(unittest.TestCase):
    def test_faulty_overloading(self):
        with self.assertRaises(TypeError):
            NoPutServiceProtocol()


class TestServiceInterface(unittest.TestCase):
    def setUp(self):
        nodeA = Node("nodeA", 0)
        nodeB = Node("nodeB", 1)
        self.nodes = [nodeA, nodeB]

        protoA = CorrectServiceProtocol()
        protoB = CorrectServiceProtocol()
        self.protocols = [protoA, protoB]

        self.magic_protocol = CorrectMagicProtocol(self.nodes)

        self.messages = defaultdict(list)

        serviceA = CorrectServiceInterface(self.nodes[0],
                                           magic=False,
                                           protocol=self.protocols[0],
                                           reaction_handler=self.handle_reactionA)
        serviceB = CorrectServiceInterface(self.nodes[1],
                                           magic=False,
                                           protocol=self.protocols[1],
                                           reaction_handler=self.handle_reactionB)
        self.services = [serviceA, serviceB]

        magic_serviceA = CorrectServiceInterface(self.nodes[0],
                                                 magic=True,
                                                 magic_protocol=self.magic_protocol,
                                                 reaction_handler=self.handle_reactionA)
        magic_serviceB = CorrectServiceInterface(self.nodes[1],
                                                 magic=True,
                                                 magic_protocol=self.magic_protocol,
                                                 reaction_handler=self.handle_reactionB)
        self.magic_services = [magic_serviceA, magic_serviceB]

        self.combined_service = CorrectServiceInterface(self.nodes[0],
                                                        magic=False,
                                                        protocol=self.protocols[0],
                                                        magic_protocol=self.magic_protocol,
                                                        reaction_handler=self.handle_reactionA)

        self.combined_magic_service = CorrectServiceInterface(self.nodes[0],
                                                              magic=True,
                                                              protocol=self.protocols[0],
                                                              magic_protocol=self.magic_protocol,
                                                              reaction_handler=self.handle_reactionA)

    def handle_reactionA(self, msg):
        self.messages[self.nodes[0].ID].append(msg)

    def handle_reactionB(self, msg):
        self.messages[self.nodes[1].ID].append(msg)

    def test_faulty_overloading(self):
        with self.assertRaises(TypeError):
            NoTypeServiceInterface(None)

        with self.assertRaises(TypeError):
            NoRequestTypeServiceInterface(None)

        with self.assertRaises(TypeError):
            NoReactionTypeServiceInterface(None)

    def test_faulty_init(self):
        with self.assertRaises(TypeError):
            CorrectServiceInterface()

    def test_default_init(self):
        # Test default arguments
        service = CorrectServiceInterface(self.nodes[0])
        self.assertIs(service.node, self.nodes[0])
        self.assertIsNone(service._protocol)
        self.assertIsNone(service._magic_protocol)
        self.assertIsNone(service._reaction_handler)
        self.assertFalse(service.is_running)
        self.assertFalse(service.is_magic)

    def test_nondefault_init(self):
        # Test arguments
        service = CorrectServiceInterface(self.nodes[0],
                                          magic=True,
                                          protocol=self.protocols[0],
                                          magic_protocol=self.magic_protocol,
                                          reaction_handler=self.handle_reactionA)
        self.assertIs(service.node, self.nodes[0])
        self.assertIs(service._protocol, self.protocols[0])
        self.assertIs(service._magic_protocol, self.magic_protocol)
        self.assertTrue(callable(service._reaction_handler))
        self.assertFalse(service.is_running)
        self.assertTrue(service.is_magic)

    def test_add_methods(self):
        # Test add methods
        service = CorrectServiceInterface(self.nodes[0])
        service.add_protocol(protocol=self.protocols[0])
        service.add_magic_protocol(protocol=self.magic_protocol)
        service.add_reaction_handler(reaction_handler=self.handle_reactionA)
        self.assertIs(service.node, self.nodes[0])
        self.assertIs(service._protocol, self.protocols[0])
        self.assertIs(service._magic_protocol, self.magic_protocol)
        self.assertTrue(callable(service._reaction_handler))
        self.assertFalse(service.is_running)
        self.assertFalse(service.is_magic)

    def test_put_normal(self):
        num_req_A = 3
        num_req_B = 4
        # Test normal protocol
        for service, protocol, num_req in zip(self.services, self.protocols, [num_req_A, num_req_B]):
            requests = list(range(num_req))
            for i in requests:
                service.put(i)
            self.assertListEqual(protocol.requests, requests)

    def test_put_magic(self):
        num_req_A = 3
        num_req_B = 4
        # Test magic protocol
        for service, node, num_req in zip(self.magic_services, self.nodes, [num_req_A, num_req_B]):
            requests = list(range(num_req))
            for i in requests:
                service.put(i)
            self.assertListEqual(self.magic_protocol.requests[node.ID], requests)

    def test_put_faulty(self):
        # Test faulty reqeust
        with self.assertRaises(ValueError):
            self.services[0].put(0.5)
        with self.assertRaises(ValueError):
            self.magic_services[0].put(0.5)

    def test_react_normal(self):
        num_req_A = 3
        num_req_B = 4
        # Test normal protocol
        for service, node, num_req in zip(self.services, self.nodes, [num_req_A, num_req_B]):
            messages = list(range(num_req))
            for i in messages:
                service.react(i)
            self.assertListEqual(self.messages[node.ID], messages)

    def test_react_magic(self):
        num_req_A = 3
        num_req_B = 4
        # Test magic protocol
        for service, node, num_req in zip(self.magic_services, self.nodes, [num_req_A, num_req_B]):
            messages = list(range(num_req))
            for i in messages:
                service.react(i)
            self.assertListEqual(self.messages[node.ID], messages)

    def test_react_faulty(self):
        # Test faulty reqeust
        with self.assertRaises(ValueError):
            self.services[0].react(0.5)
        with self.assertRaises(ValueError):
            self.magic_services[0].react(0.5)

    def test_start_normal(self):
        self.combined_service.start()
        self.assertTrue(self.combined_service.is_running)
        self.assertTrue(self.protocols[0].is_running)
        self.assertFalse(self.magic_protocol.is_running)

    def test_start_magic(self):
        self.combined_magic_service.start()
        self.assertTrue(self.combined_magic_service.is_running)
        self.assertFalse(self.protocols[0].is_running)
        self.assertTrue(self.magic_protocol.is_running)

    def test_stop_normal(self):
        self.combined_service.start()
        self.combined_service.stop()
        self.assertFalse(self.protocols[0].is_running)
        self.assertFalse(self.magic_protocol.is_running)

    def test_stop_magic(self):
        self.combined_magic_service.start()
        self.combined_magic_service.stop()
        self.assertFalse(self.protocols[0].is_running)
        self.assertFalse(self.magic_protocol.is_running)


if __name__ == '__main__':
    unittest.main()
